﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class Search
    {

        List<List<string>> fleetAttributes;

        Fleet fleet = new Fleet();

        public Search()
        {
            fleet = new Fleet();
            fleetAttributes = new List<List<string>>();
            for (int k = 0; k < fleet.GetFleet().Count; k++)
            {
                List<string> currentCarAttributes = fleet.GetFleet()[k].GetAttributeList().ConvertAll(x => x.ToLower());
                fleetAttributes.Add(currentCarAttributes);
            }
        }

        public Fleet Fleet
        {
            get => default;
            set
            {
            }
        }

        public List<string> getSearchAttributes(string query)
        {
            List<string> queryList = new List<string>();
            string[] delimiter = { " and ", " or " };
            queryList = query.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries).ToList();

            return queryList;
        }

        public List<string> getOperatorList(string query)
        {
            List<string> operaterList = new List<string>();
            operaterList = query.Split(' ').Where(s => (s == "and" || s == "or")).ToList();

            return operaterList;
        }

        public List<Vehicle> OneParamSearch(string attribute, int min, int max)
        {
            List<Vehicle> singleResult = new List<Vehicle>();

            for (int i = 0; i < fleet.GetFleet().Count; i++)
            {
                if (fleet.GetFleet()[i].GetAttributeList().ConvertAll(x => x.ToLower()).Contains(attribute)
                    && (min <= fleet.GetFleet()[i].DailyRate && fleet.GetFleet()[i].DailyRate <= max))
                {
                    singleResult.Add(fleet.GetFleet()[i]);
                } 
            }
            return singleResult;
        }

        public List<List<Vehicle>> ParamSearch(List<string> queryList, int min, int max)
        {

            List<List<Vehicle>> eachresult = new List<List<Vehicle>>();
            for (int i = 0; i < queryList.Count; i++)
            {
                eachresult.Add(OneParamSearch(queryList[i], min, max));
            }
            return eachresult;
        }

        public List<Vehicle> SearchResult(List<List<Vehicle>> eachresult, List<string> operaterList)
        {

            List<Vehicle> current_result = new List<Vehicle>();
            if (eachresult.Count == 1)
            {
                return eachresult[0];
            }
            else if (eachresult.Count == 0)
            {
                return current_result;

            }
            else
            {
                if (operaterList[0] == "or")
                {
                    current_result.AddRange(eachresult[0]);
                    current_result.AddRange(eachresult[1]);
                    current_result = current_result.Distinct().ToList();
                }
                else if (operaterList[0] == "and")
                {
                    current_result.AddRange(eachresult[0]);
                    current_result = current_result.Intersect(eachresult[1]).ToList();
                }

                eachresult.RemoveAt(0);
                eachresult.RemoveAt(0);
                operaterList.RemoveAt(0); 
                eachresult.Insert(0, current_result);
                return SearchResult(eachresult, operaterList);
            } 
        } 
    } 
}
