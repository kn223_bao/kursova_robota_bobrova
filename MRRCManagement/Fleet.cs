﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Classes
{

    public class Fleet
    {
        public List<Vehicle> auto;
        public List<string> rentedID;
        public List<int> rentedUser;

        private string fleetFile = @"..\..\..\Data\fleet.csv";
        private string rentalsFile = @"..\..\..\Data\rentals.csv";


        public Fleet()
        {        
            LoadFromFile();
        }


        public bool AddVehicle(Vehicle vehicle)
        {
            if (!auto.Any()) 
            { 
                auto.Add(vehicle);
                return true;
            }
            if (auto.Exists(x => x.VehicleRego != vehicle.VehicleRego))
            {
                auto.Add(vehicle);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RemoveVehicle(Vehicle vehicle)
        {
            for (int j = 0; j < rentedID.Count; j++)
            {
                if (vehicle.VehicleRego == rentedID[j])
                {
                    return false;
                }
            }

            for (int i = 0; i < auto.Count; i++)
            {
                if (auto[i].VehicleRego == vehicle.VehicleRego)
                {
                    auto.Remove(auto[i]);
                    return true;
                }
            }

            return false;

        }


        public bool RemoveVehicle(string vehicleRego)
        {
            for (int j = 0; j < rentedID.Count; j++)
            {
                if (vehicleRego == rentedID[j])
                {
                    return false;
                }
            }
            for (int i = 0; i < auto.Count; i++)
            {
                if (auto[i].VehicleRego == vehicleRego)
                {
                    auto.Remove(auto[i]);
                    return true;
                } 
            }
            return false;
        } 
        public bool PresentInFleet(string vehicleRego)
        {
            for (int i = 0; i < auto.Count; i++)
            {
                if (auto[i].VehicleRego == vehicleRego)
                {
                    return true;
                } 
            } 
            return false;
        }

        public List<Vehicle> GetFleet()
        {
            return auto;
        }

        public bool IsRented(string vehicleRego)
        {
            for (int i = 0; i < rentedID.Count; i++)
            {
                if (rentedID[i] == vehicleRego)
                {
                    return true;
                }
            }
            return false;
        } 

        public bool IsRenting(int customerID)
        {
            for (int i = 0; i < rentedUser.Count; i++)
            {
                if (rentedUser[i] == customerID)
                {
                    return true;
                }
            }
            return false;
        }

        public int RentedBy(string vehicleRego)
        {
            for (int i = 0; i < rentedID.Count; i++)
            {
                if (rentedID[i] == vehicleRego)
                {
                    return rentedUser[i];
                }
            }
            return -1;
        }

        public bool RentCar(string vehicleRego, int customerID)
        {
            if (!IsRented(vehicleRego))
            {
                rentedID.Add(vehicleRego);
                rentedUser.Add(customerID);
                return true;
            }            
            return false;
        }

        public int ReturnCar(string vehicleRego)
        {
            int rentedCustomerID;

            for (int i = 0; i < rentedID.Count; i++)
            {
                if (rentedID[i] == vehicleRego)
                {
                    rentedCustomerID = rentedUser[i];
                    rentedID.Remove(rentedID[i]);
                    rentedUser.Remove(rentedUser[i]);
                    return rentedCustomerID;
                }
            }
            return -1;
        }

        public string ToCSVString(string vehicleRego, int customerID)
        {
            string CSV = string.Format("{0},{1}", vehicleRego, customerID.ToString());
            return CSV;
        }

        public void SaveToFile()
        {
            File.Delete(fleetFile);
            File.Delete(rentalsFile);

            File.AppendAllText(fleetFile, string.Format("Rego", "Make", "Model", "Year", "Vehicle Class", "Num Seats", "Transmission", "Fuel", "GPS", "Sunroof", "Colour", "Daily Rate") + Environment.NewLine);
            File.AppendAllText(rentalsFile, string.Format("Vehicle", "Customer") + Environment.NewLine);

            for (int i = 0; i < auto.Count; i++)
            {
                File.AppendAllText(fleetFile, auto[i].ToCSVString());
            }

            for (int i = 0; i < rentedID.Count; i++)
            {
                File.AppendAllText(rentalsFile, ToCSVString(rentedID[i], rentedUser[i]));
            }
        }

        public void LoadFromFile()
        {
            auto = new List<Vehicle>();
            rentedID = new List<string>();
            rentedUser = new List<int>();

            if (File.Exists(fleetFile))
            {
                using (StreamReader sr = new StreamReader(fleetFile))
                {
                    sr.ReadLine();
                    while (sr.Peek() != -1)
                    {
                        string line = sr.ReadLine();
                        List<string> lineValues = line.Split(',').ToList();
                        string vehicleRego = lineValues[0];
                        string make = lineValues[1];
                        string model = lineValues[2];
                        int year = int.Parse(lineValues[3]);
                        Vehicle.AutoClass vehicleClass = (Vehicle.AutoClass)Enum.Parse(typeof(Vehicle.AutoClass), lineValues[4]);
                        int numSeats = int.Parse(lineValues[5]);
                        Vehicle.TypeOfTransmission transmissionType = (Vehicle.TypeOfTransmission)Enum.Parse(typeof(Vehicle.TypeOfTransmission), lineValues[6]);
                        Vehicle.TypeOfFuel fuelType = (Vehicle.TypeOfFuel)Enum.Parse(typeof(Vehicle.TypeOfFuel), lineValues[7]);
                        bool GPS = bool.Parse(lineValues[8]);
                        bool sunRoof = bool.Parse(lineValues[9]);
                        string colour = lineValues[10];
                        double dailyRate = double.Parse(lineValues[11]);

                        Vehicle current_vehicle = new Vehicle(vehicleRego, make, model, year, vehicleClass, numSeats,
                            transmissionType, fuelType, GPS, sunRoof, colour, dailyRate);

                        auto.Add(current_vehicle);
                    }
                }
            }

            if (File.Exists(rentalsFile))
            {
                using (StreamReader sr = new StreamReader(rentalsFile))
                {
                    sr.ReadLine();
                    while (sr.Peek() != -1)
                    {
                        string line = sr.ReadLine();
                        List<string> lineValues = line.Split(',').ToList();
                        string vehicleRego = lineValues[0];
                        int customerID = int.Parse(lineValues[1]);

                        rentedID.Add(vehicleRego);

                        rentedUser.Add(customerID); 
                    }
                }
            }
        }

    }
}
