﻿using System.Collections.Generic;

namespace Classes
{

    public class Vehicle
    {
        public enum AutoClass
        {
            Economy,
            Family,
            Luxury,
            Commercial
        }

        public enum TypeOfFuel
        {
            Petrol,
            Diesel
        }

        public enum TypeOfTransmission
        {
            Automatic,
            Manual
        }

        public AutoClass Vehicle_Class { get; set; }
        public TypeOfTransmission Transmission_Type { get; set; }
        public TypeOfFuel Fuel_Type { get; set; }
        public string VehicleRego { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Colour { get; set; }
        public int Year { get; set; }
        public int NumSeats { get; set; }
        public bool GPS { get; set; }
        public bool SunRoof { get; set; }
        public double DailyRate { get; set; }


        public Vehicle(string vehicleRego, string make, string model, int year, AutoClass vehicleClass, int numSeats,
                       TypeOfTransmission transmissionType, TypeOfFuel fuelType, bool GPS, bool sunRoof, string colour, double dailyRate)
        {
            Vehicle_Class = vehicleClass;
            VehicleRego = vehicleRego;
            Make = make;
            Model = model;
            Year = year;
            NumSeats = numSeats;
            Transmission_Type = transmissionType;
            Fuel_Type = fuelType;
            this.GPS = GPS;
            SunRoof = sunRoof;
            DailyRate = dailyRate;
            Colour = colour;

            if (NumSeats.Equals(0))
            {
                NumSeats = 1;
            }
            if (dailyRate.Equals(0))
            {
                DailyRate = 50;
            }
            if (colour.Equals(""))
            {
                Colour = "Black";
            }
            if (vehicleClass == AutoClass.Commercial)
            {
                Fuel_Type = TypeOfFuel.Diesel;
                DailyRate = 130;
            }
            else if (vehicleClass == AutoClass.Economy)
            {
                Transmission_Type = TypeOfTransmission.Automatic;
            }
            else if (vehicleClass == AutoClass.Family)
            {
                DailyRate = 80;
            }
            else
            {
                GPS = true;
                SunRoof = true;
                DailyRate = 120;
            }
        }

        public string ToCSVString()
        {
            string CSV = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}\n", VehicleRego, Make, Model,
                                       Year, Vehicle_Class, NumSeats, Transmission_Type, Fuel_Type, GPS, SunRoof, Colour, DailyRate);

            return CSV;
        } 

        public override string ToString()
        {
            return base.ToString();
        }

        public List<string> GetAttributeList()
        {

            List<string> attributeList = new List<string>
            {
                VehicleRego,
                Make,
                Model,
                Year.ToString(),
                Vehicle_Class.ToString(),
                Transmission_Type.ToString(),
                string.Format("{0}-Seater", NumSeats.ToString()),
                Colour,
                DailyRate.ToString(),
                Fuel_Type.ToString()
            };
            if (GPS)
            {
                attributeList.Add("GPS");
            }
            if (SunRoof)
            {
                attributeList.Add("sunroof");
            }

            return attributeList;
        }

    }
}
