﻿
namespace Classes
{

    public class Customer
    {

        public enum Gender
        {
            Male,
            Female
        }

        public int CustomerID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public Gender Gender_ { get; set; }

        public Customer(int customerID, string title, string firstName, string lastName, Gender gender, string dateOfBirth)
        {
            CustomerID = customerID;
            Title = title;
            FirstName = firstName;
            LastName = lastName;
            Gender_ = gender;
            DateOfBirth = dateOfBirth;
        } 
        public string ToCSVString()
        {
            string CSV = string.Format("{0},{1},{2},{3},{4},{5}\n", CustomerID, Title, FirstName, LastName, Gender_, DateOfBirth);
            return CSV;
        } 
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
