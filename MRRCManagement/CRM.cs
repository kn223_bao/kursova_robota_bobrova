﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Classes
{
    public class CRM
    {

        public List<Customer> Customers;
        private string CustomerFile = @"..\..\..\Data\customer.csv";

        public CRM()
        {
            if (File.Exists(CustomerFile))
            {
                LoadFromFile();
            }
            else
            {
                Customers = new List<Customer>();
            }
        }

        public Customer Customer
        {
            get => default;
            set
            {
            }
        }

        public bool AddCustomer(Customer customer)
        {
            if (Customers.Exists(x => x.CustomerID != customer.CustomerID))
            {
                Customers.Add(customer);
                return true;
            }
            else
            {
                return false;
            } 
        }

        public bool removecustomer(Customer customer, Fleet fleet)
        {
            if (fleet.IsRenting(customer.CustomerID) == false)
            {
                Customers.Remove(customer);
                return true;
            }
            return false;
        }

        public bool removecustomer(int customerid, Fleet fleet)
        {
            if (fleet.IsRenting(customerid) == false)
            {
                Customers.Remove(Customers.Find(x => x.CustomerID == customerid));
                return true;
            }
            return false;
        }

        public List<Customer> GetCustomers()
        {
            return Customers;
        }

        public void SaveToFile()
        {
            File.Delete(CustomerFile);
            File.AppendAllText(CustomerFile, string.Format("Customer", "Title", "Firstname", "Lastname", "Gender", "DOB") + Environment.NewLine);

            for (int i = 0; i < Customers.Count; i++)
            {
                File.AppendAllText(CustomerFile, Customers[i].ToCSVString());
            }
        }

        public void LoadFromFile()
        {
            Customers = new List<Customer>();

            using (StreamReader sr = new StreamReader(CustomerFile))
            {
                sr.ReadLine();
                while (sr.Peek() != -1)
                {
                    string line = sr.ReadLine();
                    List<string> lineValues = line.Split(',').ToList();
                    int customerID = int.Parse(lineValues[0]);
                    string title = lineValues[1];
                    string firstName = lineValues[2];
                    string lastName = lineValues[3];
                    Customer.Gender gender = (Customer.Gender)Enum.Parse(typeof(Customer.Gender), lineValues[4]);
                    string dateOfBirth = lineValues[5];
                    Customer current_customer = new Customer(customerID, title, firstName, lastName, gender, dateOfBirth);
                    Customers.Add(current_customer);
                }
            }
        }
    }
}
