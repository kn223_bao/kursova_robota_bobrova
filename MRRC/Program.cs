﻿using System;
using System.Windows.Forms;

namespace IPSCARS
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.Run(new Form1());
        }
    }
}
