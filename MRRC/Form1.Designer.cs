﻿using Microsoft.Win32;

namespace IPSCARS
{
    partial class Form1
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpFleet = new System.Windows.Forms.TabPage();
            this.gbFleetAddVehicle = new System.Windows.Forms.GroupBox();
            this.lblFleetClassInvalid = new System.Windows.Forms.Label();
            this.lblFleetModelInvalid = new System.Windows.Forms.Label();
            this.lblFleetMakeInvalid = new System.Windows.Forms.Label();
            this.lblFleetYearInvalid = new System.Windows.Forms.Label();
            this.lblFleetRegoInvalid = new System.Windows.Forms.Label();
            this.cbFleetGPS = new System.Windows.Forms.CheckBox();
            this.cbFleetSunroof = new System.Windows.Forms.CheckBox();
            this.txtFleetColour = new System.Windows.Forms.TextBox();
            this.cmbFleetFuel = new System.Windows.Forms.ComboBox();
            this.cmbFleetTransmission = new System.Windows.Forms.ComboBox();
            this.txtFleetYear = new System.Windows.Forms.TextBox();
            this.txtFleetModel = new System.Windows.Forms.TextBox();
            this.cmbFleetClass = new System.Windows.Forms.ComboBox();
            this.txtFleetMake = new System.Windows.Forms.TextBox();
            this.txtFleetRego = new System.Windows.Forms.TextBox();
            this.nudFleetDailyRate = new System.Windows.Forms.NumericUpDown();
            this.nudFleetSeats = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnFleetSubmit = new System.Windows.Forms.Button();
            this.btnFleetCancel = new System.Windows.Forms.Button();
            this.gbFleetModify = new System.Windows.Forms.GroupBox();
            this.btnFleetAdd = new System.Windows.Forms.Button();
            this.btnFleetRemove = new System.Windows.Forms.Button();
            this.btnFleetModify = new System.Windows.Forms.Button();
            this.dgvAuto = new System.Windows.Forms.DataGridView();
            this.dgvFleetRego = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetMake = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetYear = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetVehicleClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetNumSeats = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetTransmission = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetFuel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetGPS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvFleetSunroof = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvFleetDailyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFleetColor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCustomers = new System.Windows.Forms.TabPage();
            this.gbCustomerAddCustomer = new System.Windows.Forms.GroupBox();
            this.lblCustomerDOBInvalid = new System.Windows.Forms.Label();
            this.lblCustomerGenderInvalid = new System.Windows.Forms.Label();
            this.lblCustomerLastnameInvalid = new System.Windows.Forms.Label();
            this.cmbCustomerGender = new System.Windows.Forms.ComboBox();
            this.lblCustomerGender = new System.Windows.Forms.Label();
            this.txtCustomerDOB = new System.Windows.Forms.TextBox();
            this.lblCustomerDOB = new System.Windows.Forms.Label();
            this.cmbCustomerTitle = new System.Windows.Forms.ComboBox();
            this.lblCustomerTitleInvalid = new System.Windows.Forms.Label();
            this.lblCustomerFirstnameInvalid = new System.Windows.Forms.Label();
            this.txtCustomerLastname = new System.Windows.Forms.TextBox();
            this.txtCustomerFirstname = new System.Windows.Forms.TextBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.lblCustomerLastname = new System.Windows.Forms.Label();
            this.lblCustomerFirstname = new System.Windows.Forms.Label();
            this.lblCustomerTitle = new System.Windows.Forms.Label();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.btnCustomerSubmit = new System.Windows.Forms.Button();
            this.btnCustomerCancel = new System.Windows.Forms.Button();
            this.gbCustomerModifyCustomers = new System.Windows.Forms.GroupBox();
            this.btnCustomerAdd = new System.Windows.Forms.Button();
            this.btnCustomerRemove = new System.Windows.Forms.Button();
            this.btnCustomerModify = new System.Windows.Forms.Button();
            this.dgvCustomer = new System.Windows.Forms.DataGridView();
            this.dgvCustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomerTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomerFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomerLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomerGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvCustomerDOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpReport = new System.Windows.Forms.TabPage();
            this.gbReportModifyRentals = new System.Windows.Forms.GroupBox();
            this.lblReportTotalDailyRentalCharge = new System.Windows.Forms.Label();
            this.lblReportTotalVehiclesFound = new System.Windows.Forms.Label();
            this.btnReportReturnVehicle = new System.Windows.Forms.Button();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.dgvReportRego = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvReportCustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvReportDailyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpSearch = new System.Windows.Forms.TabPage();
            this.gbSearchResult = new System.Windows.Forms.GroupBox();
            this.lblSearchNoMatchingVehicleFound = new System.Windows.Forms.Label();
            this.dgvSearch = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbSearchCreateRental = new System.Windows.Forms.GroupBox();
            this.btnSearchRent = new System.Windows.Forms.Button();
            this.nudSearchRentalDuration = new System.Windows.Forms.NumericUpDown();
            this.cmbSearchCustomer = new System.Windows.Forms.ComboBox();
            this.lblSearchTotalCost = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudSearchDailyCostRangeUpper = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nudSearchDailyCostRangeLower = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchShowAll = new System.Windows.Forms.Button();
            this.btnSearchSearch = new System.Windows.Forms.Button();
            this.cmbSearchEnterQueryHere = new System.Windows.Forms.TextBox();
            this.lblFormTitle = new System.Windows.Forms.Label();
            this.lblFormSubTitle = new System.Windows.Forms.Label();
            this.ttFleetRego = new System.Windows.Forms.ToolTip(this.components);
            this.ttFleetMake = new System.Windows.Forms.ToolTip(this.components);
            this.ttFleetModel = new System.Windows.Forms.ToolTip(this.components);
            this.ttFleetClass = new System.Windows.Forms.ToolTip(this.components);
            this.ttFleetYear = new System.Windows.Forms.ToolTip(this.components);
            this.ttCustomerDOB = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.tpFleet.SuspendLayout();
            this.gbFleetAddVehicle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFleetDailyRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFleetSeats)).BeginInit();
            this.gbFleetModify.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuto)).BeginInit();
            this.tpCustomers.SuspendLayout();
            this.gbCustomerAddCustomer.SuspendLayout();
            this.gbCustomerModifyCustomers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.tpReport.SuspendLayout();
            this.gbReportModifyRentals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.tpSearch.SuspendLayout();
            this.gbSearchResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).BeginInit();
            this.gbSearchCreateRental.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchRentalDuration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchDailyCostRangeUpper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchDailyCostRangeLower)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpFleet);
            this.tabControl1.Controls.Add(this.tpCustomers);
            this.tabControl1.Controls.Add(this.tpReport);
            this.tabControl1.Controls.Add(this.tpSearch);
            this.tabControl1.Location = new System.Drawing.Point(14, 118);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1210, 653);
            this.tabControl1.TabIndex = 0;
            // 
            // tpFleet
            // 
            this.tpFleet.Controls.Add(this.gbFleetAddVehicle);
            this.tpFleet.Controls.Add(this.gbFleetModify);
            this.tpFleet.Controls.Add(this.dgvAuto);
            this.tpFleet.Location = new System.Drawing.Point(4, 25);
            this.tpFleet.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpFleet.Name = "tpFleet";
            this.tpFleet.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpFleet.Size = new System.Drawing.Size(1202, 624);
            this.tpFleet.TabIndex = 0;
            this.tpFleet.Text = "Fleet";
            this.tpFleet.UseVisualStyleBackColor = true;
            // 
            // gbFleetAddVehicle
            // 
            this.gbFleetAddVehicle.Controls.Add(this.lblFleetClassInvalid);
            this.gbFleetAddVehicle.Controls.Add(this.lblFleetModelInvalid);
            this.gbFleetAddVehicle.Controls.Add(this.lblFleetMakeInvalid);
            this.gbFleetAddVehicle.Controls.Add(this.lblFleetYearInvalid);
            this.gbFleetAddVehicle.Controls.Add(this.lblFleetRegoInvalid);
            this.gbFleetAddVehicle.Controls.Add(this.cbFleetGPS);
            this.gbFleetAddVehicle.Controls.Add(this.cbFleetSunroof);
            this.gbFleetAddVehicle.Controls.Add(this.txtFleetColour);
            this.gbFleetAddVehicle.Controls.Add(this.cmbFleetFuel);
            this.gbFleetAddVehicle.Controls.Add(this.cmbFleetTransmission);
            this.gbFleetAddVehicle.Controls.Add(this.txtFleetYear);
            this.gbFleetAddVehicle.Controls.Add(this.txtFleetModel);
            this.gbFleetAddVehicle.Controls.Add(this.cmbFleetClass);
            this.gbFleetAddVehicle.Controls.Add(this.txtFleetMake);
            this.gbFleetAddVehicle.Controls.Add(this.txtFleetRego);
            this.gbFleetAddVehicle.Controls.Add(this.nudFleetDailyRate);
            this.gbFleetAddVehicle.Controls.Add(this.nudFleetSeats);
            this.gbFleetAddVehicle.Controls.Add(this.label14);
            this.gbFleetAddVehicle.Controls.Add(this.label15);
            this.gbFleetAddVehicle.Controls.Add(this.label16);
            this.gbFleetAddVehicle.Controls.Add(this.label17);
            this.gbFleetAddVehicle.Controls.Add(this.label10);
            this.gbFleetAddVehicle.Controls.Add(this.label11);
            this.gbFleetAddVehicle.Controls.Add(this.label12);
            this.gbFleetAddVehicle.Controls.Add(this.label13);
            this.gbFleetAddVehicle.Controls.Add(this.label9);
            this.gbFleetAddVehicle.Controls.Add(this.label8);
            this.gbFleetAddVehicle.Controls.Add(this.label7);
            this.gbFleetAddVehicle.Controls.Add(this.label6);
            this.gbFleetAddVehicle.Controls.Add(this.btnFleetSubmit);
            this.gbFleetAddVehicle.Controls.Add(this.btnFleetCancel);
            this.gbFleetAddVehicle.Location = new System.Drawing.Point(0, 348);
            this.gbFleetAddVehicle.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbFleetAddVehicle.Name = "gbFleetAddVehicle";
            this.gbFleetAddVehicle.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbFleetAddVehicle.Size = new System.Drawing.Size(1200, 188);
            this.gbFleetAddVehicle.TabIndex = 2;
            this.gbFleetAddVehicle.TabStop = false;
            this.gbFleetAddVehicle.Text = "Add Vehicle";
            this.gbFleetAddVehicle.Visible = false;
            this.gbFleetAddVehicle.MouseHover += new System.EventHandler(this.gbFleetAddVehicle_MouseHover);
            // 
            // lblFleetClassInvalid
            // 
            this.lblFleetClassInvalid.AutoSize = true;
            this.lblFleetClassInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblFleetClassInvalid.Location = new System.Drawing.Point(248, 138);
            this.lblFleetClassInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFleetClassInvalid.Name = "lblFleetClassInvalid";
            this.lblFleetClassInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblFleetClassInvalid.TabIndex = 32;
            this.lblFleetClassInvalid.Text = "!";
            // 
            // lblFleetModelInvalid
            // 
            this.lblFleetModelInvalid.AutoSize = true;
            this.lblFleetModelInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblFleetModelInvalid.Location = new System.Drawing.Point(248, 108);
            this.lblFleetModelInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFleetModelInvalid.Name = "lblFleetModelInvalid";
            this.lblFleetModelInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblFleetModelInvalid.TabIndex = 31;
            this.lblFleetModelInvalid.Text = "!";
            // 
            // lblFleetMakeInvalid
            // 
            this.lblFleetMakeInvalid.AutoSize = true;
            this.lblFleetMakeInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblFleetMakeInvalid.Location = new System.Drawing.Point(248, 72);
            this.lblFleetMakeInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFleetMakeInvalid.Name = "lblFleetMakeInvalid";
            this.lblFleetMakeInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblFleetMakeInvalid.TabIndex = 30;
            this.lblFleetMakeInvalid.Text = "!";
            // 
            // lblFleetYearInvalid
            // 
            this.lblFleetYearInvalid.AutoSize = true;
            this.lblFleetYearInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblFleetYearInvalid.Location = new System.Drawing.Point(494, 38);
            this.lblFleetYearInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFleetYearInvalid.Name = "lblFleetYearInvalid";
            this.lblFleetYearInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblFleetYearInvalid.TabIndex = 29;
            this.lblFleetYearInvalid.Text = "!";
            // 
            // lblFleetRegoInvalid
            // 
            this.lblFleetRegoInvalid.AutoSize = true;
            this.lblFleetRegoInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblFleetRegoInvalid.Location = new System.Drawing.Point(198, 40);
            this.lblFleetRegoInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFleetRegoInvalid.Name = "lblFleetRegoInvalid";
            this.lblFleetRegoInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblFleetRegoInvalid.TabIndex = 3;
            this.lblFleetRegoInvalid.Text = "!";
            // 
            // cbFleetGPS
            // 
            this.cbFleetGPS.AutoSize = true;
            this.cbFleetGPS.Location = new System.Drawing.Point(808, 39);
            this.cbFleetGPS.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cbFleetGPS.Name = "cbFleetGPS";
            this.cbFleetGPS.Size = new System.Drawing.Size(18, 17);
            this.cbFleetGPS.TabIndex = 28;
            this.cbFleetGPS.UseVisualStyleBackColor = true;
            // 
            // cbFleetSunroof
            // 
            this.cbFleetSunroof.AutoSize = true;
            this.cbFleetSunroof.Location = new System.Drawing.Point(692, 42);
            this.cbFleetSunroof.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cbFleetSunroof.Name = "cbFleetSunroof";
            this.cbFleetSunroof.Size = new System.Drawing.Size(18, 17);
            this.cbFleetSunroof.TabIndex = 27;
            this.cbFleetSunroof.UseVisualStyleBackColor = true;
            // 
            // txtFleetColour
            // 
            this.txtFleetColour.Location = new System.Drawing.Point(692, 70);
            this.txtFleetColour.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFleetColour.Name = "txtFleetColour";
            this.txtFleetColour.Size = new System.Drawing.Size(136, 22);
            this.txtFleetColour.TabIndex = 26;
            // 
            // cmbFleetFuel
            // 
            this.cmbFleetFuel.FormattingEnabled = true;
            this.cmbFleetFuel.Location = new System.Drawing.Point(400, 107);
            this.cmbFleetFuel.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cmbFleetFuel.Name = "cmbFleetFuel";
            this.cmbFleetFuel.Size = new System.Drawing.Size(136, 24);
            this.cmbFleetFuel.TabIndex = 25;
            // 
            // cmbFleetTransmission
            // 
            this.cmbFleetTransmission.FormattingEnabled = true;
            this.cmbFleetTransmission.Location = new System.Drawing.Point(400, 70);
            this.cmbFleetTransmission.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cmbFleetTransmission.Name = "cmbFleetTransmission";
            this.cmbFleetTransmission.Size = new System.Drawing.Size(136, 24);
            this.cmbFleetTransmission.TabIndex = 24;
            // 
            // txtFleetYear
            // 
            this.txtFleetYear.Location = new System.Drawing.Point(400, 38);
            this.txtFleetYear.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFleetYear.Name = "txtFleetYear";
            this.txtFleetYear.Size = new System.Drawing.Size(86, 22);
            this.txtFleetYear.TabIndex = 23;
            this.ttFleetYear.SetToolTip(this.txtFleetYear, "Required field. Please pick a year in the format YYYY from 1800-2018");
            this.txtFleetYear.TextChanged += new System.EventHandler(this.txtFleetYear_TextChanged);
            // 
            // txtFleetModel
            // 
            this.txtFleetModel.Location = new System.Drawing.Point(104, 106);
            this.txtFleetModel.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFleetModel.Name = "txtFleetModel";
            this.txtFleetModel.Size = new System.Drawing.Size(136, 22);
            this.txtFleetModel.TabIndex = 22;
            this.ttFleetMake.SetToolTip(this.txtFleetModel, "Required field. e.g. Sudan");
            this.txtFleetModel.TextChanged += new System.EventHandler(this.txtFleetModel_TextChanged);
            // 
            // cmbFleetClass
            // 
            this.cmbFleetClass.FormattingEnabled = true;
            this.cmbFleetClass.Location = new System.Drawing.Point(104, 137);
            this.cmbFleetClass.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cmbFleetClass.Name = "cmbFleetClass";
            this.cmbFleetClass.Size = new System.Drawing.Size(136, 24);
            this.cmbFleetClass.TabIndex = 21;
            this.ttFleetClass.SetToolTip(this.cmbFleetClass, "Required field. Please pix one of the options form the drop down menu");
            this.cmbFleetClass.SelectionChangeCommitted += new System.EventHandler(this.cmbFleetClass_SelectionChangeCommitted);
            // 
            // txtFleetMake
            // 
            this.txtFleetMake.Location = new System.Drawing.Point(104, 70);
            this.txtFleetMake.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFleetMake.Name = "txtFleetMake";
            this.txtFleetMake.Size = new System.Drawing.Size(136, 22);
            this.txtFleetMake.TabIndex = 20;
            this.ttFleetMake.SetToolTip(this.txtFleetMake, "Required field.  e.g. \"Honda\"");
            this.txtFleetMake.TextChanged += new System.EventHandler(this.txtFleetMake_TextChanged);
            // 
            // txtFleetRego
            // 
            this.txtFleetRego.Location = new System.Drawing.Point(104, 38);
            this.txtFleetRego.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtFleetRego.Name = "txtFleetRego";
            this.txtFleetRego.Size = new System.Drawing.Size(86, 22);
            this.txtFleetRego.TabIndex = 19;
            this.ttFleetRego.SetToolTip(this.txtFleetRego, "Required field. Must be 3 numbers followed by 3 capital letters");
            this.txtFleetRego.TextChanged += new System.EventHandler(this.txtFleetRego_TextChanged);
            // 
            // nudFleetDailyRate
            // 
            this.nudFleetDailyRate.Location = new System.Drawing.Point(692, 106);
            this.nudFleetDailyRate.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nudFleetDailyRate.Name = "nudFleetDailyRate";
            this.nudFleetDailyRate.Size = new System.Drawing.Size(78, 22);
            this.nudFleetDailyRate.TabIndex = 18;
            // 
            // nudFleetSeats
            // 
            this.nudFleetSeats.Location = new System.Drawing.Point(400, 137);
            this.nudFleetSeats.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nudFleetSeats.Name = "nudFleetSeats";
            this.nudFleetSeats.Size = new System.Drawing.Size(60, 22);
            this.nudFleetSeats.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(740, 42);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 17);
            this.label14.TabIndex = 16;
            this.label14.Text = "GPS:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(590, 108);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 17);
            this.label15.TabIndex = 15;
            this.label15.Text = "Daily Rate:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(614, 74);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 17);
            this.label16.TabIndex = 14;
            this.label16.Text = "Colour:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(606, 42);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 17);
            this.label17.TabIndex = 13;
            this.label17.Text = "Sunroof:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(328, 138);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "Seats:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(338, 108);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 11;
            this.label11.Text = "Fuel:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(278, 71);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 17);
            this.label12.TabIndex = 10;
            this.label12.Text = "Transmission:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(336, 39);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 17);
            this.label13.TabIndex = 9;
            this.label13.Text = "Year:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(42, 138);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Class:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(36, 108);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Model:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(42, 74);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Make:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(42, 38);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Rego:";
            // 
            // btnFleetSubmit
            // 
            this.btnFleetSubmit.Enabled = false;
            this.btnFleetSubmit.Location = new System.Drawing.Point(942, 145);
            this.btnFleetSubmit.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFleetSubmit.Name = "btnFleetSubmit";
            this.btnFleetSubmit.Size = new System.Drawing.Size(118, 31);
            this.btnFleetSubmit.TabIndex = 4;
            this.btnFleetSubmit.Text = "Submit";
            this.btnFleetSubmit.UseVisualStyleBackColor = true;
            this.btnFleetSubmit.Click += new System.EventHandler(this.btnFleetSubmit_Click);
            // 
            // btnFleetCancel
            // 
            this.btnFleetCancel.Location = new System.Drawing.Point(1064, 145);
            this.btnFleetCancel.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFleetCancel.Name = "btnFleetCancel";
            this.btnFleetCancel.Size = new System.Drawing.Size(118, 31);
            this.btnFleetCancel.TabIndex = 3;
            this.btnFleetCancel.Text = "Cancel";
            this.btnFleetCancel.UseVisualStyleBackColor = true;
            this.btnFleetCancel.Click += new System.EventHandler(this.btnFleetCancel_Click);
            // 
            // gbFleetModify
            // 
            this.gbFleetModify.Controls.Add(this.btnFleetAdd);
            this.gbFleetModify.Controls.Add(this.btnFleetRemove);
            this.gbFleetModify.Controls.Add(this.btnFleetModify);
            this.gbFleetModify.Location = new System.Drawing.Point(2, 550);
            this.gbFleetModify.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbFleetModify.Name = "gbFleetModify";
            this.gbFleetModify.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbFleetModify.Size = new System.Drawing.Size(1194, 70);
            this.gbFleetModify.TabIndex = 1;
            this.gbFleetModify.TabStop = false;
            this.gbFleetModify.Text = "Modify fleet";
            // 
            // btnFleetAdd
            // 
            this.btnFleetAdd.Location = new System.Drawing.Point(1062, 28);
            this.btnFleetAdd.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFleetAdd.Name = "btnFleetAdd";
            this.btnFleetAdd.Size = new System.Drawing.Size(118, 31);
            this.btnFleetAdd.TabIndex = 2;
            this.btnFleetAdd.Text = "Add";
            this.btnFleetAdd.UseVisualStyleBackColor = true;
            this.btnFleetAdd.Click += new System.EventHandler(this.btnFleetAdd_Click);
            // 
            // btnFleetRemove
            // 
            this.btnFleetRemove.Location = new System.Drawing.Point(142, 28);
            this.btnFleetRemove.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFleetRemove.Name = "btnFleetRemove";
            this.btnFleetRemove.Size = new System.Drawing.Size(118, 31);
            this.btnFleetRemove.TabIndex = 1;
            this.btnFleetRemove.Text = "Remove";
            this.btnFleetRemove.UseVisualStyleBackColor = true;
            this.btnFleetRemove.Click += new System.EventHandler(this.btnFleetRemove_Click);
            // 
            // btnFleetModify
            // 
            this.btnFleetModify.Location = new System.Drawing.Point(14, 28);
            this.btnFleetModify.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnFleetModify.Name = "btnFleetModify";
            this.btnFleetModify.Size = new System.Drawing.Size(118, 31);
            this.btnFleetModify.TabIndex = 0;
            this.btnFleetModify.Text = "Modify";
            this.btnFleetModify.UseVisualStyleBackColor = true;
            this.btnFleetModify.Click += new System.EventHandler(this.btnFleetModify_Click);
            // 
            // dgvAuto
            // 
            this.dgvAuto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Beige;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAuto.BackgroundColor = System.Drawing.Color.Bisque;
            this.dgvAuto.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAuto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAuto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvFleetRego,
            this.dgvFleetMake,
            this.dgvFleetModel,
            this.dgvFleetYear,
            this.dgvFleetVehicleClass,
            this.dgvFleetNumSeats,
            this.dgvFleetTransmission,
            this.dgvFleetFuel,
            this.dgvFleetGPS,
            this.dgvFleetSunroof,
            this.dgvFleetDailyRate,
            this.dgvFleetColor});
            this.dgvAuto.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvAuto.EnableHeadersVisualStyles = false;
            this.dgvAuto.Location = new System.Drawing.Point(2, 1);
            this.dgvAuto.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvAuto.MultiSelect = false;
            this.dgvAuto.Name = "dgvAuto";
            this.dgvAuto.ReadOnly = true;
            this.dgvAuto.RowHeadersWidth = 51;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAuto.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAuto.RowTemplate.Height = 40;
            this.dgvAuto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAuto.Size = new System.Drawing.Size(1198, 532);
            this.dgvAuto.TabIndex = 0;
            this.dgvAuto.SelectionChanged += new System.EventHandler(this.dgvFleet_SelectionChanged);
            // 
            // dgvFleetRego
            // 
            this.dgvFleetRego.HeaderText = "Rego";
            this.dgvFleetRego.MinimumWidth = 6;
            this.dgvFleetRego.Name = "dgvFleetRego";
            this.dgvFleetRego.ReadOnly = true;
            // 
            // dgvFleetMake
            // 
            this.dgvFleetMake.HeaderText = "Make";
            this.dgvFleetMake.MinimumWidth = 6;
            this.dgvFleetMake.Name = "dgvFleetMake";
            this.dgvFleetMake.ReadOnly = true;
            // 
            // dgvFleetModel
            // 
            this.dgvFleetModel.HeaderText = "Model";
            this.dgvFleetModel.MinimumWidth = 6;
            this.dgvFleetModel.Name = "dgvFleetModel";
            this.dgvFleetModel.ReadOnly = true;
            // 
            // dgvFleetYear
            // 
            this.dgvFleetYear.HeaderText = "Year";
            this.dgvFleetYear.MinimumWidth = 6;
            this.dgvFleetYear.Name = "dgvFleetYear";
            this.dgvFleetYear.ReadOnly = true;
            // 
            // dgvFleetVehicleClass
            // 
            this.dgvFleetVehicleClass.HeaderText = "Vehicle Class";
            this.dgvFleetVehicleClass.MinimumWidth = 6;
            this.dgvFleetVehicleClass.Name = "dgvFleetVehicleClass";
            this.dgvFleetVehicleClass.ReadOnly = true;
            // 
            // dgvFleetNumSeats
            // 
            this.dgvFleetNumSeats.HeaderText = "Seats";
            this.dgvFleetNumSeats.MinimumWidth = 6;
            this.dgvFleetNumSeats.Name = "dgvFleetNumSeats";
            this.dgvFleetNumSeats.ReadOnly = true;
            // 
            // dgvFleetTransmission
            // 
            this.dgvFleetTransmission.HeaderText = "Transmission";
            this.dgvFleetTransmission.MinimumWidth = 6;
            this.dgvFleetTransmission.Name = "dgvFleetTransmission";
            this.dgvFleetTransmission.ReadOnly = true;
            // 
            // dgvFleetFuel
            // 
            this.dgvFleetFuel.HeaderText = "Fuel";
            this.dgvFleetFuel.MinimumWidth = 6;
            this.dgvFleetFuel.Name = "dgvFleetFuel";
            this.dgvFleetFuel.ReadOnly = true;
            // 
            // dgvFleetGPS
            // 
            this.dgvFleetGPS.HeaderText = "GPS";
            this.dgvFleetGPS.MinimumWidth = 6;
            this.dgvFleetGPS.Name = "dgvFleetGPS";
            this.dgvFleetGPS.ReadOnly = true;
            // 
            // dgvFleetSunroof
            // 
            this.dgvFleetSunroof.HeaderText = "Sunroof";
            this.dgvFleetSunroof.MinimumWidth = 6;
            this.dgvFleetSunroof.Name = "dgvFleetSunroof";
            this.dgvFleetSunroof.ReadOnly = true;
            // 
            // dgvFleetDailyRate
            // 
            this.dgvFleetDailyRate.HeaderText = "Daily Rate";
            this.dgvFleetDailyRate.MinimumWidth = 6;
            this.dgvFleetDailyRate.Name = "dgvFleetDailyRate";
            this.dgvFleetDailyRate.ReadOnly = true;
            this.dgvFleetDailyRate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFleetDailyRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvFleetColor
            // 
            this.dgvFleetColor.HeaderText = "Color";
            this.dgvFleetColor.MinimumWidth = 6;
            this.dgvFleetColor.Name = "dgvFleetColor";
            this.dgvFleetColor.ReadOnly = true;
            this.dgvFleetColor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFleetColor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tpCustomers
            // 
            this.tpCustomers.Controls.Add(this.gbCustomerAddCustomer);
            this.tpCustomers.Controls.Add(this.gbCustomerModifyCustomers);
            this.tpCustomers.Controls.Add(this.dgvCustomer);
            this.tpCustomers.Location = new System.Drawing.Point(4, 25);
            this.tpCustomers.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpCustomers.Name = "tpCustomers";
            this.tpCustomers.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpCustomers.Size = new System.Drawing.Size(1202, 624);
            this.tpCustomers.TabIndex = 1;
            this.tpCustomers.Text = "Customers";
            this.tpCustomers.UseVisualStyleBackColor = true;
            // 
            // gbCustomerAddCustomer
            // 
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerDOBInvalid);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerGenderInvalid);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerLastnameInvalid);
            this.gbCustomerAddCustomer.Controls.Add(this.cmbCustomerGender);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerGender);
            this.gbCustomerAddCustomer.Controls.Add(this.txtCustomerDOB);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerDOB);
            this.gbCustomerAddCustomer.Controls.Add(this.cmbCustomerTitle);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerTitleInvalid);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerFirstnameInvalid);
            this.gbCustomerAddCustomer.Controls.Add(this.txtCustomerLastname);
            this.gbCustomerAddCustomer.Controls.Add(this.txtCustomerFirstname);
            this.gbCustomerAddCustomer.Controls.Add(this.txtCustomerID);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerLastname);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerFirstname);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerTitle);
            this.gbCustomerAddCustomer.Controls.Add(this.lblCustomerID);
            this.gbCustomerAddCustomer.Controls.Add(this.btnCustomerSubmit);
            this.gbCustomerAddCustomer.Controls.Add(this.btnCustomerCancel);
            this.gbCustomerAddCustomer.Location = new System.Drawing.Point(0, 415);
            this.gbCustomerAddCustomer.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbCustomerAddCustomer.Name = "gbCustomerAddCustomer";
            this.gbCustomerAddCustomer.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbCustomerAddCustomer.Size = new System.Drawing.Size(1200, 126);
            this.gbCustomerAddCustomer.TabIndex = 3;
            this.gbCustomerAddCustomer.TabStop = false;
            this.gbCustomerAddCustomer.Text = "Add Customer";
            this.gbCustomerAddCustomer.Visible = false;
            this.gbCustomerAddCustomer.MouseHover += new System.EventHandler(this.gbCustomerAddCustomer_MouseHover);
            // 
            // lblCustomerDOBInvalid
            // 
            this.lblCustomerDOBInvalid.AutoSize = true;
            this.lblCustomerDOBInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerDOBInvalid.Location = new System.Drawing.Point(258, 84);
            this.lblCustomerDOBInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerDOBInvalid.Name = "lblCustomerDOBInvalid";
            this.lblCustomerDOBInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblCustomerDOBInvalid.TabIndex = 38;
            this.lblCustomerDOBInvalid.Text = "!";
            // 
            // lblCustomerGenderInvalid
            // 
            this.lblCustomerGenderInvalid.AutoSize = true;
            this.lblCustomerGenderInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerGenderInvalid.Location = new System.Drawing.Point(469, 84);
            this.lblCustomerGenderInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerGenderInvalid.Name = "lblCustomerGenderInvalid";
            this.lblCustomerGenderInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblCustomerGenderInvalid.TabIndex = 37;
            this.lblCustomerGenderInvalid.Text = "!";
            // 
            // lblCustomerLastnameInvalid
            // 
            this.lblCustomerLastnameInvalid.AutoSize = true;
            this.lblCustomerLastnameInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerLastnameInvalid.Location = new System.Drawing.Point(800, 84);
            this.lblCustomerLastnameInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerLastnameInvalid.Name = "lblCustomerLastnameInvalid";
            this.lblCustomerLastnameInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblCustomerLastnameInvalid.TabIndex = 36;
            this.lblCustomerLastnameInvalid.Text = "!";
            // 
            // cmbCustomerGender
            // 
            this.cmbCustomerGender.FormattingEnabled = true;
            this.cmbCustomerGender.Location = new System.Drawing.Point(362, 82);
            this.cmbCustomerGender.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomerGender.Name = "cmbCustomerGender";
            this.cmbCustomerGender.Size = new System.Drawing.Size(92, 24);
            this.cmbCustomerGender.TabIndex = 35;
            this.cmbCustomerGender.SelectionChangeCommitted += new System.EventHandler(this.cmbCustomerGender_SelectionChangeCommitted);
            // 
            // lblCustomerGender
            // 
            this.lblCustomerGender.AutoSize = true;
            this.lblCustomerGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerGender.Location = new System.Drawing.Point(286, 85);
            this.lblCustomerGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerGender.Name = "lblCustomerGender";
            this.lblCustomerGender.Size = new System.Drawing.Size(72, 17);
            this.lblCustomerGender.TabIndex = 34;
            this.lblCustomerGender.Text = "Gender: ";
            // 
            // txtCustomerDOB
            // 
            this.txtCustomerDOB.Location = new System.Drawing.Point(135, 82);
            this.txtCustomerDOB.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerDOB.Name = "txtCustomerDOB";
            this.txtCustomerDOB.Size = new System.Drawing.Size(115, 22);
            this.txtCustomerDOB.TabIndex = 33;
            this.ttCustomerDOB.SetToolTip(this.txtCustomerDOB, "Required field. Please pick a year in the format DD/MM/YYYY");
            this.txtCustomerDOB.TextChanged += new System.EventHandler(this.txtCustomerDOB_TextChanged);
            // 
            // lblCustomerDOB
            // 
            this.lblCustomerDOB.AutoSize = true;
            this.lblCustomerDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerDOB.Location = new System.Drawing.Point(74, 85);
            this.lblCustomerDOB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerDOB.Name = "lblCustomerDOB";
            this.lblCustomerDOB.Size = new System.Drawing.Size(46, 17);
            this.lblCustomerDOB.TabIndex = 32;
            this.lblCustomerDOB.Text = "DOB:";
            // 
            // cmbCustomerTitle
            // 
            this.cmbCustomerTitle.FormattingEnabled = true;
            this.cmbCustomerTitle.Location = new System.Drawing.Point(362, 34);
            this.cmbCustomerTitle.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomerTitle.Name = "cmbCustomerTitle";
            this.cmbCustomerTitle.Size = new System.Drawing.Size(62, 24);
            this.cmbCustomerTitle.TabIndex = 31;
            this.cmbCustomerTitle.SelectionChangeCommitted += new System.EventHandler(this.cmbCustomerTitle_SelectionChangeCommitted);
            // 
            // lblCustomerTitleInvalid
            // 
            this.lblCustomerTitleInvalid.AutoSize = true;
            this.lblCustomerTitleInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerTitleInvalid.Location = new System.Drawing.Point(434, 36);
            this.lblCustomerTitleInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerTitleInvalid.Name = "lblCustomerTitleInvalid";
            this.lblCustomerTitleInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblCustomerTitleInvalid.TabIndex = 30;
            this.lblCustomerTitleInvalid.Text = "!";
            // 
            // lblCustomerFirstnameInvalid
            // 
            this.lblCustomerFirstnameInvalid.AutoSize = true;
            this.lblCustomerFirstnameInvalid.ForeColor = System.Drawing.Color.Red;
            this.lblCustomerFirstnameInvalid.Location = new System.Drawing.Point(800, 34);
            this.lblCustomerFirstnameInvalid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerFirstnameInvalid.Name = "lblCustomerFirstnameInvalid";
            this.lblCustomerFirstnameInvalid.Size = new System.Drawing.Size(10, 16);
            this.lblCustomerFirstnameInvalid.TabIndex = 29;
            this.lblCustomerFirstnameInvalid.Text = "!";
            // 
            // txtCustomerLastname
            // 
            this.txtCustomerLastname.Location = new System.Drawing.Point(612, 82);
            this.txtCustomerLastname.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtCustomerLastname.Name = "txtCustomerLastname";
            this.txtCustomerLastname.Size = new System.Drawing.Size(174, 22);
            this.txtCustomerLastname.TabIndex = 26;
            this.txtCustomerLastname.TextChanged += new System.EventHandler(this.txtCustomerLastname_TextChanged);
            // 
            // txtCustomerFirstname
            // 
            this.txtCustomerFirstname.Location = new System.Drawing.Point(612, 33);
            this.txtCustomerFirstname.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtCustomerFirstname.Name = "txtCustomerFirstname";
            this.txtCustomerFirstname.Size = new System.Drawing.Size(174, 22);
            this.txtCustomerFirstname.TabIndex = 23;
            this.txtCustomerFirstname.TextChanged += new System.EventHandler(this.txtCustomerFirstname_TextChanged);
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Location = new System.Drawing.Point(135, 36);
            this.txtCustomerID.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(34, 22);
            this.txtCustomerID.TabIndex = 19;
            // 
            // lblCustomerLastname
            // 
            this.lblCustomerLastname.AutoSize = true;
            this.lblCustomerLastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerLastname.Location = new System.Drawing.Point(512, 85);
            this.lblCustomerLastname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerLastname.Name = "lblCustomerLastname";
            this.lblCustomerLastname.Size = new System.Drawing.Size(90, 17);
            this.lblCustomerLastname.TabIndex = 14;
            this.lblCustomerLastname.Text = "Last Name:";
            // 
            // lblCustomerFirstname
            // 
            this.lblCustomerFirstname.AutoSize = true;
            this.lblCustomerFirstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerFirstname.Location = new System.Drawing.Point(510, 36);
            this.lblCustomerFirstname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerFirstname.Name = "lblCustomerFirstname";
            this.lblCustomerFirstname.Size = new System.Drawing.Size(91, 17);
            this.lblCustomerFirstname.TabIndex = 9;
            this.lblCustomerFirstname.Text = "First Name:";
            // 
            // lblCustomerTitle
            // 
            this.lblCustomerTitle.AutoSize = true;
            this.lblCustomerTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerTitle.Location = new System.Drawing.Point(307, 36);
            this.lblCustomerTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerTitle.Name = "lblCustomerTitle";
            this.lblCustomerTitle.Size = new System.Drawing.Size(45, 17);
            this.lblCustomerTitle.TabIndex = 6;
            this.lblCustomerTitle.Text = "Title:";
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomerID.Location = new System.Drawing.Point(26, 38);
            this.lblCustomerID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(96, 17);
            this.lblCustomerID.TabIndex = 5;
            this.lblCustomerID.Text = "CustomerID:";
            // 
            // btnCustomerSubmit
            // 
            this.btnCustomerSubmit.Enabled = false;
            this.btnCustomerSubmit.Location = new System.Drawing.Point(944, 77);
            this.btnCustomerSubmit.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnCustomerSubmit.Name = "btnCustomerSubmit";
            this.btnCustomerSubmit.Size = new System.Drawing.Size(118, 31);
            this.btnCustomerSubmit.TabIndex = 4;
            this.btnCustomerSubmit.Text = "Submit";
            this.btnCustomerSubmit.UseVisualStyleBackColor = true;
            this.btnCustomerSubmit.Click += new System.EventHandler(this.btnCustomerSubmit_Click);
            // 
            // btnCustomerCancel
            // 
            this.btnCustomerCancel.Location = new System.Drawing.Point(1065, 77);
            this.btnCustomerCancel.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnCustomerCancel.Name = "btnCustomerCancel";
            this.btnCustomerCancel.Size = new System.Drawing.Size(118, 31);
            this.btnCustomerCancel.TabIndex = 3;
            this.btnCustomerCancel.Text = "Cancel";
            this.btnCustomerCancel.UseVisualStyleBackColor = true;
            this.btnCustomerCancel.Click += new System.EventHandler(this.btnCustomerCancel_Click);
            // 
            // gbCustomerModifyCustomers
            // 
            this.gbCustomerModifyCustomers.Controls.Add(this.btnCustomerAdd);
            this.gbCustomerModifyCustomers.Controls.Add(this.btnCustomerRemove);
            this.gbCustomerModifyCustomers.Controls.Add(this.btnCustomerModify);
            this.gbCustomerModifyCustomers.Location = new System.Drawing.Point(4, 543);
            this.gbCustomerModifyCustomers.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbCustomerModifyCustomers.Name = "gbCustomerModifyCustomers";
            this.gbCustomerModifyCustomers.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbCustomerModifyCustomers.Size = new System.Drawing.Size(1194, 70);
            this.gbCustomerModifyCustomers.TabIndex = 2;
            this.gbCustomerModifyCustomers.TabStop = false;
            this.gbCustomerModifyCustomers.Text = "Modify Customers";
            // 
            // btnCustomerAdd
            // 
            this.btnCustomerAdd.Location = new System.Drawing.Point(1062, 28);
            this.btnCustomerAdd.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnCustomerAdd.Name = "btnCustomerAdd";
            this.btnCustomerAdd.Size = new System.Drawing.Size(118, 31);
            this.btnCustomerAdd.TabIndex = 2;
            this.btnCustomerAdd.Text = "Add";
            this.btnCustomerAdd.UseVisualStyleBackColor = true;
            this.btnCustomerAdd.Click += new System.EventHandler(this.btnCustomerAdd_Click);
            // 
            // btnCustomerRemove
            // 
            this.btnCustomerRemove.Enabled = false;
            this.btnCustomerRemove.Location = new System.Drawing.Point(142, 28);
            this.btnCustomerRemove.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnCustomerRemove.Name = "btnCustomerRemove";
            this.btnCustomerRemove.Size = new System.Drawing.Size(118, 31);
            this.btnCustomerRemove.TabIndex = 1;
            this.btnCustomerRemove.Text = "Remove";
            this.btnCustomerRemove.UseVisualStyleBackColor = true;
            this.btnCustomerRemove.Click += new System.EventHandler(this.btnCustomerRemove_Click);
            // 
            // btnCustomerModify
            // 
            this.btnCustomerModify.Location = new System.Drawing.Point(14, 28);
            this.btnCustomerModify.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnCustomerModify.Name = "btnCustomerModify";
            this.btnCustomerModify.Size = new System.Drawing.Size(118, 31);
            this.btnCustomerModify.TabIndex = 0;
            this.btnCustomerModify.Text = "Modify";
            this.btnCustomerModify.UseVisualStyleBackColor = true;
            this.btnCustomerModify.Click += new System.EventHandler(this.btnCustomerModify_Click);
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Beige;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustomer.BackgroundColor = System.Drawing.Color.Bisque;
            this.dgvCustomer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvCustomerID,
            this.dgvCustomerTitle,
            this.dgvCustomerFirstName,
            this.dgvCustomerLastName,
            this.dgvCustomerGender,
            this.dgvCustomerDOB});
            this.dgvCustomer.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvCustomer.EnableHeadersVisualStyles = false;
            this.dgvCustomer.Location = new System.Drawing.Point(2, 1);
            this.dgvCustomer.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.RowHeadersWidth = 51;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustomer.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCustomer.RowTemplate.Height = 40;
            this.dgvCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomer.Size = new System.Drawing.Size(1198, 538);
            this.dgvCustomer.TabIndex = 1;
            this.dgvCustomer.SelectionChanged += new System.EventHandler(this.dgvCustomer_SelectionChanged);
            // 
            // dgvCustomerID
            // 
            this.dgvCustomerID.HeaderText = "CustomerID";
            this.dgvCustomerID.MinimumWidth = 6;
            this.dgvCustomerID.Name = "dgvCustomerID";
            this.dgvCustomerID.ReadOnly = true;
            // 
            // dgvCustomerTitle
            // 
            this.dgvCustomerTitle.HeaderText = "Title";
            this.dgvCustomerTitle.MinimumWidth = 6;
            this.dgvCustomerTitle.Name = "dgvCustomerTitle";
            this.dgvCustomerTitle.ReadOnly = true;
            // 
            // dgvCustomerFirstName
            // 
            this.dgvCustomerFirstName.HeaderText = "First Name";
            this.dgvCustomerFirstName.MinimumWidth = 6;
            this.dgvCustomerFirstName.Name = "dgvCustomerFirstName";
            this.dgvCustomerFirstName.ReadOnly = true;
            // 
            // dgvCustomerLastName
            // 
            this.dgvCustomerLastName.HeaderText = "Last Name";
            this.dgvCustomerLastName.MinimumWidth = 6;
            this.dgvCustomerLastName.Name = "dgvCustomerLastName";
            this.dgvCustomerLastName.ReadOnly = true;
            // 
            // dgvCustomerGender
            // 
            this.dgvCustomerGender.HeaderText = "Gender";
            this.dgvCustomerGender.MinimumWidth = 6;
            this.dgvCustomerGender.Name = "dgvCustomerGender";
            this.dgvCustomerGender.ReadOnly = true;
            // 
            // dgvCustomerDOB
            // 
            this.dgvCustomerDOB.HeaderText = "DOB";
            this.dgvCustomerDOB.MinimumWidth = 6;
            this.dgvCustomerDOB.Name = "dgvCustomerDOB";
            this.dgvCustomerDOB.ReadOnly = true;
            // 
            // tpReport
            // 
            this.tpReport.Controls.Add(this.gbReportModifyRentals);
            this.tpReport.Controls.Add(this.dgvReport);
            this.tpReport.Location = new System.Drawing.Point(4, 25);
            this.tpReport.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpReport.Name = "tpReport";
            this.tpReport.Size = new System.Drawing.Size(1202, 624);
            this.tpReport.TabIndex = 2;
            this.tpReport.Text = "Rented";
            this.tpReport.UseVisualStyleBackColor = true;
            // 
            // gbReportModifyRentals
            // 
            this.gbReportModifyRentals.Controls.Add(this.lblReportTotalDailyRentalCharge);
            this.gbReportModifyRentals.Controls.Add(this.lblReportTotalVehiclesFound);
            this.gbReportModifyRentals.Controls.Add(this.btnReportReturnVehicle);
            this.gbReportModifyRentals.Location = new System.Drawing.Point(6, 540);
            this.gbReportModifyRentals.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbReportModifyRentals.Name = "gbReportModifyRentals";
            this.gbReportModifyRentals.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbReportModifyRentals.Size = new System.Drawing.Size(1194, 70);
            this.gbReportModifyRentals.TabIndex = 3;
            this.gbReportModifyRentals.TabStop = false;
            this.gbReportModifyRentals.Text = "Modify Rentals";
            // 
            // lblReportTotalDailyRentalCharge
            // 
            this.lblReportTotalDailyRentalCharge.AutoSize = true;
            this.lblReportTotalDailyRentalCharge.Location = new System.Drawing.Point(956, 36);
            this.lblReportTotalDailyRentalCharge.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReportTotalDailyRentalCharge.Name = "lblReportTotalDailyRentalCharge";
            this.lblReportTotalDailyRentalCharge.Size = new System.Drawing.Size(157, 16);
            this.lblReportTotalDailyRentalCharge.TabIndex = 2;
            this.lblReportTotalDailyRentalCharge.Text = "Total daily rental charge: ";
            // 
            // lblReportTotalVehiclesFound
            // 
            this.lblReportTotalVehiclesFound.AutoSize = true;
            this.lblReportTotalVehiclesFound.Location = new System.Drawing.Point(772, 36);
            this.lblReportTotalVehiclesFound.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReportTotalVehiclesFound.Name = "lblReportTotalVehiclesFound";
            this.lblReportTotalVehiclesFound.Size = new System.Drawing.Size(138, 16);
            this.lblReportTotalVehiclesFound.TabIndex = 1;
            this.lblReportTotalVehiclesFound.Text = "Total vehicles rented: ";
            // 
            // btnReportReturnVehicle
            // 
            this.btnReportReturnVehicle.Location = new System.Drawing.Point(14, 28);
            this.btnReportReturnVehicle.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnReportReturnVehicle.Name = "btnReportReturnVehicle";
            this.btnReportReturnVehicle.Size = new System.Drawing.Size(118, 31);
            this.btnReportReturnVehicle.TabIndex = 0;
            this.btnReportReturnVehicle.Text = "Return Vehicle";
            this.btnReportReturnVehicle.UseVisualStyleBackColor = true;
            this.btnReportReturnVehicle.Click += new System.EventHandler(this.btnReportReturnVehicle_Click);
            // 
            // dgvReport
            // 
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Beige;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.BackgroundColor = System.Drawing.Color.Bisque;
            this.dgvReport.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvReportRego,
            this.dgvReportCustomerID,
            this.dgvReportDailyRate});
            this.dgvReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvReport.EnableHeadersVisualStyles = false;
            this.dgvReport.Location = new System.Drawing.Point(0, 0);
            this.dgvReport.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.RowHeadersWidth = 51;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvReport.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvReport.RowTemplate.Height = 40;
            this.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReport.Size = new System.Drawing.Size(1202, 538);
            this.dgvReport.TabIndex = 2;
            this.dgvReport.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport_CellContentClick);
            this.dgvReport.SelectionChanged += new System.EventHandler(this.dgvReport_SelectionChanged);
            // 
            // dgvReportRego
            // 
            this.dgvReportRego.HeaderText = "Vehicle Rego";
            this.dgvReportRego.MinimumWidth = 6;
            this.dgvReportRego.Name = "dgvReportRego";
            this.dgvReportRego.ReadOnly = true;
            // 
            // dgvReportCustomerID
            // 
            this.dgvReportCustomerID.HeaderText = "Customer ID";
            this.dgvReportCustomerID.MinimumWidth = 6;
            this.dgvReportCustomerID.Name = "dgvReportCustomerID";
            this.dgvReportCustomerID.ReadOnly = true;
            // 
            // dgvReportDailyRate
            // 
            this.dgvReportDailyRate.HeaderText = "Daily Rate";
            this.dgvReportDailyRate.MinimumWidth = 6;
            this.dgvReportDailyRate.Name = "dgvReportDailyRate";
            this.dgvReportDailyRate.ReadOnly = true;
            // 
            // tpSearch
            // 
            this.tpSearch.Controls.Add(this.gbSearchResult);
            this.tpSearch.Controls.Add(this.nudSearchDailyCostRangeUpper);
            this.tpSearch.Controls.Add(this.label2);
            this.tpSearch.Controls.Add(this.nudSearchDailyCostRangeLower);
            this.tpSearch.Controls.Add(this.label1);
            this.tpSearch.Controls.Add(this.btnSearchShowAll);
            this.tpSearch.Controls.Add(this.btnSearchSearch);
            this.tpSearch.Controls.Add(this.cmbSearchEnterQueryHere);
            this.tpSearch.Location = new System.Drawing.Point(4, 25);
            this.tpSearch.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.tpSearch.Name = "tpSearch";
            this.tpSearch.Size = new System.Drawing.Size(1202, 624);
            this.tpSearch.TabIndex = 3;
            this.tpSearch.Text = "Search";
            this.tpSearch.UseVisualStyleBackColor = true;
            // 
            // gbSearchResult
            // 
            this.gbSearchResult.Controls.Add(this.lblSearchNoMatchingVehicleFound);
            this.gbSearchResult.Controls.Add(this.dgvSearch);
            this.gbSearchResult.Controls.Add(this.gbSearchCreateRental);
            this.gbSearchResult.Location = new System.Drawing.Point(10, 102);
            this.gbSearchResult.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbSearchResult.Name = "gbSearchResult";
            this.gbSearchResult.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbSearchResult.Size = new System.Drawing.Size(1182, 512);
            this.gbSearchResult.TabIndex = 7;
            this.gbSearchResult.TabStop = false;
            this.gbSearchResult.Text = "Results";
            // 
            // lblSearchNoMatchingVehicleFound
            // 
            this.lblSearchNoMatchingVehicleFound.AutoSize = true;
            this.lblSearchNoMatchingVehicleFound.Location = new System.Drawing.Point(403, 201);
            this.lblSearchNoMatchingVehicleFound.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchNoMatchingVehicleFound.Name = "lblSearchNoMatchingVehicleFound";
            this.lblSearchNoMatchingVehicleFound.Size = new System.Drawing.Size(338, 64);
            this.lblSearchNoMatchingVehicleFound.TabIndex = 2;
            this.lblSearchNoMatchingVehicleFound.Text = "No matching vehicles found... Please try another search.";
            this.lblSearchNoMatchingVehicleFound.Visible = false;
            // 
            // dgvSearch
            // 
            this.dgvSearch.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Beige;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.1F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSearch.BackgroundColor = System.Drawing.Color.Bisque;
            this.dgvSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSearch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10});
            this.dgvSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvSearch.EnableHeadersVisualStyles = false;
            this.dgvSearch.Location = new System.Drawing.Point(2, 16);
            this.dgvSearch.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.dgvSearch.Name = "dgvSearch";
            this.dgvSearch.RowHeadersWidth = 51;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSearch.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvSearch.RowTemplate.Height = 40;
            this.dgvSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSearch.Size = new System.Drawing.Size(1178, 364);
            this.dgvSearch.TabIndex = 1;
            this.dgvSearch.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Rego";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Make";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Model";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Year";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Vehicle Class";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Seats";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Transmission";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Fuel";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "GPS";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "Sunroof";
            this.dataGridViewCheckBoxColumn2.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Daily Rate";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Color";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // gbSearchCreateRental
            // 
            this.gbSearchCreateRental.Controls.Add(this.btnSearchRent);
            this.gbSearchCreateRental.Controls.Add(this.nudSearchRentalDuration);
            this.gbSearchCreateRental.Controls.Add(this.cmbSearchCustomer);
            this.gbSearchCreateRental.Controls.Add(this.lblSearchTotalCost);
            this.gbSearchCreateRental.Controls.Add(this.label4);
            this.gbSearchCreateRental.Controls.Add(this.label3);
            this.gbSearchCreateRental.Enabled = false;
            this.gbSearchCreateRental.Location = new System.Drawing.Point(682, 399);
            this.gbSearchCreateRental.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbSearchCreateRental.Name = "gbSearchCreateRental";
            this.gbSearchCreateRental.Padding = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.gbSearchCreateRental.Size = new System.Drawing.Size(482, 100);
            this.gbSearchCreateRental.TabIndex = 0;
            this.gbSearchCreateRental.TabStop = false;
            this.gbSearchCreateRental.Text = "Create Rental";
            // 
            // btnSearchRent
            // 
            this.btnSearchRent.Enabled = false;
            this.btnSearchRent.Location = new System.Drawing.Point(366, 44);
            this.btnSearchRent.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSearchRent.Name = "btnSearchRent";
            this.btnSearchRent.Size = new System.Drawing.Size(102, 39);
            this.btnSearchRent.TabIndex = 8;
            this.btnSearchRent.Text = "Rent";
            this.btnSearchRent.UseVisualStyleBackColor = true;
            this.btnSearchRent.Click += new System.EventHandler(this.btnSearchRent_Click);
            // 
            // nudSearchRentalDuration
            // 
            this.nudSearchRentalDuration.Location = new System.Drawing.Point(136, 64);
            this.nudSearchRentalDuration.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nudSearchRentalDuration.Name = "nudSearchRentalDuration";
            this.nudSearchRentalDuration.Size = new System.Drawing.Size(60, 22);
            this.nudSearchRentalDuration.TabIndex = 7;
            this.nudSearchRentalDuration.ValueChanged += new System.EventHandler(this.nudSearchRentalDuration_ValueChanged);
            // 
            // cmbSearchCustomer
            // 
            this.cmbSearchCustomer.FormattingEnabled = true;
            this.cmbSearchCustomer.Location = new System.Drawing.Point(136, 32);
            this.cmbSearchCustomer.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cmbSearchCustomer.Name = "cmbSearchCustomer";
            this.cmbSearchCustomer.Size = new System.Drawing.Size(206, 24);
            this.cmbSearchCustomer.TabIndex = 6;
            // 
            // lblSearchTotalCost
            // 
            this.lblSearchTotalCost.AutoSize = true;
            this.lblSearchTotalCost.Location = new System.Drawing.Point(214, 68);
            this.lblSearchTotalCost.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearchTotalCost.Name = "lblSearchTotalCost";
            this.lblSearchTotalCost.Size = new System.Drawing.Size(85, 16);
            this.lblSearchTotalCost.TabIndex = 5;
            this.lblSearchTotalCost.Text = "Total Cost: $-";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 68);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Duration(days):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 36);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "User:";
            // 
            // nudSearchDailyCostRangeUpper
            // 
            this.nudSearchDailyCostRangeUpper.Location = new System.Drawing.Point(239, 69);
            this.nudSearchDailyCostRangeUpper.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nudSearchDailyCostRangeUpper.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudSearchDailyCostRangeUpper.Name = "nudSearchDailyCostRangeUpper";
            this.nudSearchDailyCostRangeUpper.Size = new System.Drawing.Size(60, 22);
            this.nudSearchDailyCostRangeUpper.TabIndex = 6;
            this.nudSearchDailyCostRangeUpper.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 69);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "-";
            // 
            // nudSearchDailyCostRangeLower
            // 
            this.nudSearchDailyCostRangeLower.Location = new System.Drawing.Point(162, 68);
            this.nudSearchDailyCostRangeLower.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.nudSearchDailyCostRangeLower.Name = "nudSearchDailyCostRangeLower";
            this.nudSearchDailyCostRangeLower.Size = new System.Drawing.Size(60, 22);
            this.nudSearchDailyCostRangeLower.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Daily cost range";
            // 
            // btnSearchShowAll
            // 
            this.btnSearchShowAll.Location = new System.Drawing.Point(496, 23);
            this.btnSearchShowAll.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSearchShowAll.Name = "btnSearchShowAll";
            this.btnSearchShowAll.Size = new System.Drawing.Size(92, 36);
            this.btnSearchShowAll.TabIndex = 2;
            this.btnSearchShowAll.Text = "Show All";
            this.btnSearchShowAll.UseVisualStyleBackColor = true;
            this.btnSearchShowAll.Click += new System.EventHandler(this.btnSearchShowAll_Click);
            // 
            // btnSearchSearch
            // 
            this.btnSearchSearch.Enabled = false;
            this.btnSearchSearch.Location = new System.Drawing.Point(402, 23);
            this.btnSearchSearch.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.btnSearchSearch.Name = "btnSearchSearch";
            this.btnSearchSearch.Size = new System.Drawing.Size(92, 36);
            this.btnSearchSearch.TabIndex = 1;
            this.btnSearchSearch.Text = "Search";
            this.btnSearchSearch.UseVisualStyleBackColor = true;
            this.btnSearchSearch.Click += new System.EventHandler(this.btnSearchSearch_Click);
            // 
            // cmbSearchEnterQueryHere
            // 
            this.cmbSearchEnterQueryHere.HideSelection = false;
            this.cmbSearchEnterQueryHere.Location = new System.Drawing.Point(40, 23);
            this.cmbSearchEnterQueryHere.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.cmbSearchEnterQueryHere.Name = "cmbSearchEnterQueryHere";
            this.cmbSearchEnterQueryHere.Size = new System.Drawing.Size(300, 22);
            this.cmbSearchEnterQueryHere.TabIndex = 0;
            this.cmbSearchEnterQueryHere.Text = "Type here...";
            this.cmbSearchEnterQueryHere.Click += new System.EventHandler(this.cmbSearchEnterQueryHere_Click);
            this.cmbSearchEnterQueryHere.TextChanged += new System.EventHandler(this.cmbSearchEnterQueryHere_TextChanged);
            // 
            // lblFormTitle
            // 
            this.lblFormTitle.AutoSize = true;
            this.lblFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormTitle.Location = new System.Drawing.Point(12, 20);
            this.lblFormTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFormTitle.Name = "lblFormTitle";
            this.lblFormTitle.Size = new System.Drawing.Size(518, 42);
            this.lblFormTitle.TabIndex = 1;
            this.lblFormTitle.Text = "RENT A CAR";
            // 
            // lblFormSubTitle
            // 
            this.lblFormSubTitle.AutoSize = true;
            this.lblFormSubTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormSubTitle.Location = new System.Drawing.Point(16, 77);
            this.lblFormSubTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFormSubTitle.Name = "lblFormSubTitle";
            this.lblFormSubTitle.Size = new System.Drawing.Size(199, 25);
            this.lblFormSubTitle.TabIndex = 2;
            this.lblFormSubTitle.Text = "Worktop";
            // 
            // ttFleetRego
            // 
            this.ttFleetRego.AutoPopDelay = 5000;
            this.ttFleetRego.InitialDelay = 500;
            this.ttFleetRego.ReshowDelay = 100;
            this.ttFleetRego.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.ttFleetRego.ToolTipTitle = "Warning";
            // 
            // ttFleetMake
            // 
            this.ttFleetMake.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.ttFleetMake.ToolTipTitle = "Warning";
            // 
            // ttFleetModel
            // 
            this.ttFleetModel.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.ttFleetModel.ToolTipTitle = "Warning";
            // 
            // ttFleetClass
            // 
            this.ttFleetClass.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.ttFleetClass.ToolTipTitle = "Warning";
            // 
            // ttFleetYear
            // 
            this.ttFleetYear.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.ttFleetYear.ToolTipTitle = "Warning";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 736);
            this.Controls.Add(this.lblFormSubTitle);
            this.Controls.Add(this.lblFormTitle);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.Name = "Form1";
            this.Text = "Car Rent System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tpFleet.ResumeLayout(false);
            this.gbFleetAddVehicle.ResumeLayout(false);
            this.gbFleetAddVehicle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFleetDailyRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFleetSeats)).EndInit();
            this.gbFleetModify.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAuto)).EndInit();
            this.tpCustomers.ResumeLayout(false);
            this.gbCustomerAddCustomer.ResumeLayout(false);
            this.gbCustomerAddCustomer.PerformLayout();
            this.gbCustomerModifyCustomers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.tpReport.ResumeLayout(false);
            this.gbReportModifyRentals.ResumeLayout(false);
            this.gbReportModifyRentals.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.tpSearch.ResumeLayout(false);
            this.tpSearch.PerformLayout();
            this.gbSearchResult.ResumeLayout(false);
            this.gbSearchResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSearch)).EndInit();
            this.gbSearchCreateRental.ResumeLayout(false);
            this.gbSearchCreateRental.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchRentalDuration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchDailyCostRangeUpper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSearchDailyCostRangeLower)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpFleet;
        private System.Windows.Forms.DataGridView dgvAuto;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetRego;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetMake;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetYear;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetVehicleClass;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetNumSeats;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetTransmission;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetFuel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvFleetGPS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dgvFleetSunroof;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetDailyRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFleetColor;
        private System.Windows.Forms.TabPage tpCustomers;
        private System.Windows.Forms.TabPage tpReport;
        private System.Windows.Forms.TabPage tpSearch;
        private System.Windows.Forms.Label lblFormTitle;
        private System.Windows.Forms.Label lblFormSubTitle;
        private System.Windows.Forms.GroupBox gbFleetModify;
        private System.Windows.Forms.Button btnFleetAdd;
        private System.Windows.Forms.Button btnFleetRemove;
        private System.Windows.Forms.Button btnFleetModify;
        private System.Windows.Forms.GroupBox gbCustomerModifyCustomers;
        private System.Windows.Forms.Button btnCustomerAdd;
        private System.Windows.Forms.Button btnCustomerRemove;
        private System.Windows.Forms.Button btnCustomerModify;
        private System.Windows.Forms.DataGridView dgvCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvCustomerDOB;
        private System.Windows.Forms.GroupBox gbReportModifyRentals;
        private System.Windows.Forms.Button btnReportReturnVehicle;
        private System.Windows.Forms.DataGridView dgvReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvReportRego;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvReportCustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvReportDailyRate;
        private System.Windows.Forms.Label lblReportTotalDailyRentalCharge;
        private System.Windows.Forms.Label lblReportTotalVehiclesFound;
        private System.Windows.Forms.GroupBox gbSearchResult;
        private System.Windows.Forms.DataGridView dgvSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.GroupBox gbSearchCreateRental;
        private System.Windows.Forms.Button btnSearchRent;
        private System.Windows.Forms.NumericUpDown nudSearchRentalDuration;
        private System.Windows.Forms.ComboBox cmbSearchCustomer;
        private System.Windows.Forms.Label lblSearchTotalCost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudSearchDailyCostRangeUpper;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudSearchDailyCostRangeLower;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearchShowAll;
        private System.Windows.Forms.Button btnSearchSearch;
        private System.Windows.Forms.TextBox cmbSearchEnterQueryHere;
        private System.Windows.Forms.GroupBox gbFleetAddVehicle;
        private System.Windows.Forms.NumericUpDown nudFleetDailyRate;
        private System.Windows.Forms.NumericUpDown nudFleetSeats;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnFleetSubmit;
        private System.Windows.Forms.Button btnFleetCancel;
        private System.Windows.Forms.CheckBox cbFleetGPS;
        private System.Windows.Forms.CheckBox cbFleetSunroof;
        private System.Windows.Forms.TextBox txtFleetColour;
        private System.Windows.Forms.ComboBox cmbFleetFuel;
        private System.Windows.Forms.ComboBox cmbFleetTransmission;
        private System.Windows.Forms.TextBox txtFleetYear;
        private System.Windows.Forms.TextBox txtFleetModel;
        private System.Windows.Forms.ComboBox cmbFleetClass;
        private System.Windows.Forms.TextBox txtFleetMake;
        private System.Windows.Forms.TextBox txtFleetRego;
        private System.Windows.Forms.ToolTip ttFleetRego;
        private System.Windows.Forms.ToolTip ttFleetYear;
        private System.Windows.Forms.ToolTip ttFleetMake;
        private System.Windows.Forms.ToolTip ttFleetClass;
        private System.Windows.Forms.ToolTip ttFleetModel;
        private System.Windows.Forms.Label lblSearchNoMatchingVehicleFound;
        private System.Windows.Forms.Label lblFleetClassInvalid;
        private System.Windows.Forms.Label lblFleetModelInvalid;
        private System.Windows.Forms.Label lblFleetMakeInvalid;
        private System.Windows.Forms.Label lblFleetYearInvalid;
        private System.Windows.Forms.Label lblFleetRegoInvalid;
        private System.Windows.Forms.GroupBox gbCustomerAddCustomer;
        private System.Windows.Forms.Label lblCustomerTitleInvalid;
        private System.Windows.Forms.Label lblCustomerFirstnameInvalid;
        private System.Windows.Forms.TextBox txtCustomerLastname;
        private System.Windows.Forms.TextBox txtCustomerFirstname;
        private System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label lblCustomerLastname;
        private System.Windows.Forms.Label lblCustomerFirstname;
        private System.Windows.Forms.Label lblCustomerTitle;
        private System.Windows.Forms.Label lblCustomerID;
        private System.Windows.Forms.Button btnCustomerSubmit;
        private System.Windows.Forms.Button btnCustomerCancel;
        private System.Windows.Forms.ComboBox cmbCustomerTitle;
        private System.Windows.Forms.Label lblCustomerDOBInvalid;
        private System.Windows.Forms.Label lblCustomerGenderInvalid;
        private System.Windows.Forms.Label lblCustomerLastnameInvalid;
        private System.Windows.Forms.ComboBox cmbCustomerGender;
        private System.Windows.Forms.Label lblCustomerGender;
        private System.Windows.Forms.TextBox txtCustomerDOB;
        private System.Windows.Forms.Label lblCustomerDOB;
        private System.Windows.Forms.ToolTip ttCustomerDOB;
    }
}