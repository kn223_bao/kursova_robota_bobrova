﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Classes;

namespace IPSCARS
{
    public partial class Form1 : Form
    {
        private string[] autoCols = new string[] { "Rego", "Model", "Make", "Year", "Vehicle Class", "Seats", "Transmission", "Fuel Type",
                                                       "GPS", "Sunroof", "Daily Rate", "Colour"};
        private string[] userCols = new string[] { "CustomerID", "Title", "First Name", "Last Name", "Gender", "DOB" };

        private string[] rentalColumns = new string[] { "Vehicle Rego", "Customer ID", "Daily Rate" };

        private string selectedAutoID;
        private int selectedID;

        private static int REG_COL = 0;
        private static int ID_COL = 0;

        Fleet currentFleet = new Fleet();
        CRM currentCustomer = new CRM();
        Search currentSearch = new Search();

        public Form1()
        {                       
            InitializeComponent();
            SetComponents();
            PopulateAllDataGridViews();
        }

        private void SetComponents()
        {
            this.WindowState = FormWindowState.Maximized;
            
            SetDataGridViewColumns();            
            SetComboBoxes();
            SetNumericUpDowns();
            ClearFleetGroupBoXAttributes();

            this.dgvAuto.AllowUserToAddRows = false;
            this.dgvCustomer.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvSearch.AllowUserToAddRows = false;

            this.dgvAuto.ReadOnly = true;
            this.dgvCustomer.ReadOnly = true;
            this.dgvReport.ReadOnly = true;
            this.dgvSearch.ReadOnly = true;          
        }

        private void SetupcmbSearchCusomter()
        {
            List<Customer> customerList = currentCustomer.GetCustomers();
            cmbSearchCustomer.Items.Clear();
            for (int i = 0; i < customerList.Count; i++)
            {
                if (currentFleet.IsRenting(customerList[i].CustomerID) == false)
                {
                    cmbSearchCustomer.Items.Add(string.Format("{0} - {1} {2} {3}", customerList[i].CustomerID,
                    customerList[i].Title, customerList[i].FirstName, customerList[i].LastName));
                }
            }
        }

        private void SetNumericUpDowns()
        {           
            nudFleetSeats.Minimum = 0;
            nudFleetSeats.Maximum = 20;
            nudFleetDailyRate.Minimum = 0;
            nudFleetDailyRate.Maximum = 1000;
        }

        private void SetDataGridViewColumns()
        {
            dgvAuto.ColumnCount = autoCols.Length;
            dgvCustomer.ColumnCount = userCols.Length;

            for (int i = 0; i < autoCols.Length; i++)
            {
                dgvAuto.Columns[i].Name = autoCols[i];
            }
            for (int i = 0; i < userCols.Length; i++)
            {
                dgvCustomer.Columns[i].Name = userCols[i];
            }
        }

        private void PopulateDataGridViewFleet()
        {
            dgvAuto.Rows.Clear();

            List<Vehicle> vehicles = currentFleet.GetFleet();

            foreach (Vehicle x in vehicles)
            {
                dgvAuto.Rows.Add(x.VehicleRego, x.Make, x.Model, x.Year, x.Vehicle_Class, x.NumSeats, x.Transmission_Type, x.Fuel_Type, x.GPS, x.SunRoof, x.DailyRate, x.Colour);
            }
        }

        private void PopulateDataGridViewCustomer()
        {
            dgvCustomer.Rows.Clear();

            List<Customer> customers = currentCustomer.GetCustomers();

            foreach (Customer x in customers)
            {
                dgvCustomer.Rows.Add(x.CustomerID, x.Title, x.FirstName, x.LastName, x.Gender_, x.DateOfBirth);
            }
        }

        private void PopulateDataGridViewReport()
        {
            dgvReport.Rows.Clear();

            List<string> RentedID = currentFleet.rentedID;
            List<int> RentedUser = currentFleet.rentedUser;
            List<double> RentedDailyRate = new List<double>();

            foreach (string Rego in RentedID)
            {
                List<Vehicle> matchingCar = currentFleet.GetFleet().Where(x => x.VehicleRego == Rego).ToList();
                RentedDailyRate.Add(matchingCar[0].DailyRate);
            }

            for (int i = 0; i < RentedID.Count; i++)
            {
                dgvReport.Rows.Add(RentedID[i], RentedUser[i], string.Format("${0}", RentedDailyRate[i]));
            }
            lblReportTotalVehiclesFound.Text = string.Format("Total Vehicles Found: {0}", RentedID.Count);
            lblReportTotalDailyRentalCharge.Text = string.Format("Total daily rental charge: ${0}", RentedDailyRate.Sum());
        }

        private void PopulateAllDataGridViews()
        {
            PopulateDataGridViewFleet();
            PopulateDataGridViewCustomer();
            PopulateDataGridViewReport();
        }

        private void dgvFleet_SelectionChanged(object sender, EventArgs e)
        {
            int rowsCount = dgvAuto.SelectedRows.Count;

            if (rowsCount == 1)
            {
                selectedAutoID = (String)(dgvAuto.SelectedRows[0].Cells[REG_COL].Value);
                btnFleetRemove.Enabled = true;
                btnFleetModify.Enabled = true;
            }                                 
        }

        private void dgvCustomer_SelectionChanged(object sender, EventArgs e)
        {
            int rowsCount = dgvCustomer.SelectedRows.Count;
            if (rowsCount == 1)
            {
                selectedID = (Int32)(dgvCustomer.SelectedRows[0].Cells[ID_COL].Value);
                btnCustomerRemove.Enabled = true;
                btnCustomerModify.Enabled = true;
            }
        }


        private void SetComboBoxes()
        {
            cmbFleetClass.DataSource = Enum.GetNames(typeof(Vehicle.AutoClass));
            cmbFleetFuel.DataSource = Enum.GetNames(typeof(Vehicle.TypeOfFuel));
            cmbFleetTransmission.DataSource = Enum.GetNames(typeof(Vehicle.TypeOfTransmission));

            cmbCustomerGender.Items.Add("Male");
            cmbCustomerGender.Items.Add("Female");
            cmbCustomerTitle.Items.Add("Mr");
            cmbCustomerTitle.Items.Add("Mrs");
            cmbCustomerTitle.Items.Add("Ms");

            SetupcmbSearchCusomter();
        }

        private void btnFleetRemove_Click(object sender, EventArgs e)
        {
            DialogResult drVehicleRemoveConfirmation = MessageBox.Show(String.Format("Do you really want to remove vehicle {0}?", selectedAutoID),
                "Remove vehicle confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if(drVehicleRemoveConfirmation == DialogResult.Yes)
            {
                if (currentFleet.IsRented(selectedAutoID))
                {
                    DialogResult dbFailedToRemoveVehicle = MessageBox.Show(String.Format("Vehicle {0} is currently being rented and can not be removed", selectedAutoID),
                        "Remove vehicle error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    currentFleet.RemoveVehicle(selectedAutoID);
                    currentFleet.SaveToFile();
                    PopulateDataGridViewFleet();
                }
            }            
        }

        private void btnCustomerRemove_Click(object sender, EventArgs e)
        {
            DialogResult drCustomerRemoveConfirmationi = MessageBox.Show(String.Format("Do you really want to remove customer {0}?", selectedID),
                "Remove customer confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (drCustomerRemoveConfirmationi == DialogResult.Yes)
            {
                if (currentCustomer.removecustomer(selectedID, currentFleet))
                {
                    currentCustomer.SaveToFile();
                    SetupcmbSearchCusomter();
                    PopulateDataGridViewCustomer();
                }
                else
                {
                    DialogResult drFailedToRemoveCustomer = MessageBox.Show(String.Format("Customer {0} is currently renting a vehicle and can not be deleted", selectedID),
                    "Error removing customer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }                                
            }            
        }

        private void btnFleetModify_Click(object sender, EventArgs e)
        {
            gbFleetAddVehicle.Text = "Modify Vehicle";

            gbFleetAddVehicle.Visible = true;
            btnFleetCancel.Enabled = true;
            btnFleetSubmit.Enabled = false;
            btnFleetModify.Enabled = false;
            btnFleetRemove.Enabled = false;
            btnFleetAdd.Enabled = false;

            lblFleetRegoInvalid.Text = "";
            lblFleetMakeInvalid.Text = "";
            lblFleetModelInvalid.Text = "";
            lblFleetClassInvalid.Text = "";
            lblFleetYearInvalid.Text = "";

            gbFleetModifyVehicleSelectedVehicle();
        }

        private void btnCustomerModify_Click(object sender, EventArgs e)
        {
            gbCustomerAddCustomer.Text = "Modify Customer";

            gbCustomerAddCustomer.Visible = true;
            btnCustomerCancel.Enabled = true;
            btnCustomerSubmit.Enabled = true;
            btnCustomerModify.Enabled = false;
            btnCustomerRemove.Enabled = false;
            btnCustomerAdd.Enabled = false;

            lblCustomerFirstnameInvalid.Text = "";
            lblCustomerLastnameInvalid.Text = "";
            lblCustomerDOBInvalid.Text = "";
            lblCustomerTitleInvalid.Text = "";
            lblCustomerGenderInvalid.Text = "";

            gbCustomerModifyVehicleSelectedVehicle();
        }

        public void gbFleetModifyVehicleSelectedVehicle()
        {
            btnFleetSubmit.Enabled = true;
            txtFleetRego.Enabled = false;
            txtFleetRego.Text = (string)(dgvAuto.SelectedRows[0].Cells[REG_COL].Value);
            txtFleetMake.Text = (string)(dgvAuto.SelectedRows[0].Cells[1].Value);
            txtFleetModel.Text = (string)(dgvAuto.SelectedRows[0].Cells[2].Value);
            txtFleetYear.Text = ((int)(dgvAuto.SelectedRows[0].Cells[3].Value)).ToString();
            cmbFleetClass.SelectedIndex = cmbFleetClass.FindStringExact((dgvAuto.SelectedRows[0].Cells[4].Value).ToString());
            nudFleetSeats.Value = (int)(dgvAuto.SelectedRows[0].Cells[5].Value);
            cmbFleetTransmission.SelectedIndex = cmbFleetTransmission.FindStringExact((dgvAuto.SelectedRows[0].Cells[6].Value).ToString());
            cmbFleetFuel.SelectedIndex = cmbFleetFuel.FindStringExact((dgvAuto.SelectedRows[0].Cells[7].Value).ToString());
            cbFleetGPS.Checked = (bool)(dgvAuto.SelectedRows[0].Cells[8].Value);
            cbFleetSunroof.Checked = (bool)(dgvAuto.SelectedRows[0].Cells[9].Value);
            nudFleetDailyRate.Value = decimal.Parse(dgvAuto.SelectedRows[0].Cells[10].Value.ToString());
            txtFleetColour.Text = (string)(dgvAuto.SelectedRows[0].Cells[11].Value);
        }


        public void gbCustomerModifyVehicleSelectedVehicle()
        {
            btnCustomerSubmit.Enabled = true;
            txtCustomerID.Enabled = false;
            txtCustomerID.Text = ((int)(dgvCustomer.SelectedRows[0].Cells[0].Value)).ToString();
            cmbCustomerTitle.SelectedIndex = cmbCustomerTitle.FindStringExact((dgvCustomer.SelectedRows[0].Cells[1].Value).ToString());
            txtCustomerFirstname.Text = (string)(dgvCustomer.SelectedRows[0].Cells[2].Value);
            txtCustomerLastname.Text = (string)(dgvCustomer.SelectedRows[0].Cells[3].Value);
            cmbCustomerGender.SelectedIndex = cmbCustomerGender.FindStringExact((dgvCustomer.SelectedRows[0].Cells[4].Value).ToString());
            txtCustomerDOB.Text = (string)(dgvCustomer.SelectedRows[0].Cells[5].Value);
        }


        private void btnFleetCancel_Click(object sender, EventArgs e)
        {
            gbFleetAddVehicle.Visible = false;
            btnFleetModify.Enabled = true;
            btnFleetRemove.Enabled = true;
            btnFleetAdd.Enabled = true;

            ClearFleetGroupBoXAttributes();
        }

        private void btnCustomerCancel_Click(object sender, EventArgs e)
        {
            gbCustomerAddCustomer.Visible = false;
            btnCustomerModify.Enabled = true;
            btnCustomerRemove.Enabled = true;
            btnCustomerAdd.Enabled = true;

            ClearCustomerGroupBoxAttributes();
        }


        private void btnFleetAdd_Click(object sender, EventArgs e)
        {
            txtFleetRego.Enabled = true;
            gbFleetAddVehicle.Text = "Add Vehicle";
            gbFleetAddVehicle.Visible = true;
            btnFleetCancel.Enabled = true;
            btnFleetSubmit.Enabled = false;
            btnFleetModify.Enabled = false;
            btnFleetRemove.Enabled = false;
            btnFleetAdd.Enabled = false;

            lblFleetRegoInvalid.Text = "!";
            lblFleetRegoInvalid.ForeColor = Color.Red;
            lblFleetMakeInvalid.Text = "!";
            lblFleetMakeInvalid.ForeColor = Color.Red;
            lblFleetModelInvalid.Text = "!";
            lblFleetModelInvalid.ForeColor = Color.Red;
            lblFleetClassInvalid.Text = "!";
            lblFleetClassInvalid.ForeColor = Color.Red;
            lblFleetYearInvalid.Text = "!";
            lblFleetYearInvalid.ForeColor = Color.Red;
        }


        private void btnCustomerAdd_Click(object sender, EventArgs e)
        {
            int lastRow = dgvCustomer.Rows.Count - 1;

            int currentID = (int)(dgvCustomer.Rows[lastRow].Cells[0].Value);
            currentID++;
            string currentStringID = currentID.ToString();
            txtCustomerID.Text = currentStringID;

            gbCustomerAddCustomer.Text = "Add Customer";

            txtCustomerID.Enabled = false;
            gbCustomerAddCustomer.Visible = true;
            btnCustomerCancel.Enabled = true;
            btnCustomerSubmit.Enabled = false;
            btnCustomerRemove.Enabled = false;
            btnCustomerModify.Enabled = false;
            btnCustomerAdd.Enabled = true;
         
            lblCustomerTitleInvalid.Text = "!";
            lblCustomerTitleInvalid.ForeColor = Color.Red;
            lblCustomerFirstnameInvalid.Text = "!";
            lblCustomerFirstnameInvalid.ForeColor = Color.Red;
            lblCustomerLastnameInvalid.Text = "!";
            lblCustomerLastnameInvalid.ForeColor = Color.Red;
            lblCustomerGenderInvalid.Text = "!";
            lblCustomerGenderInvalid.ForeColor = Color.Red;
            lblCustomerDOBInvalid.Text = "!";
            lblCustomerDOBInvalid.ForeColor = Color.Red;
        }

        private void ClearFleetGroupBoXAttributes()
        {
            txtFleetRego.Clear();
            txtFleetMake.Clear();
            txtFleetModel.Clear();
            txtFleetYear.Clear();
            cmbFleetClass.SelectedIndex = -1;
            nudFleetSeats.ResetText();
            cmbFleetTransmission.SelectedIndex = 0;
            cmbFleetFuel.SelectedIndex = 0;
            cbFleetGPS.Checked = false;
            cbFleetSunroof.Checked = false;
            nudFleetDailyRate.ResetText();
            txtFleetColour.Clear();
        }

        private void ClearCustomerGroupBoxAttributes()
        {
            txtCustomerID.Clear();
            cmbCustomerTitle.SelectedIndex = -1;
            txtCustomerFirstname.Clear();
            txtCustomerLastname.Clear();
            cmbCustomerGender.SelectedIndex = -1;
            txtCustomerDOB.Clear();
        }


        public bool ValidInputsFleet()
        {
            if(lblFleetRegoInvalid.Text == "" && lblFleetMakeInvalid.Text == "" && lblFleetModelInvalid.Text == "" 
                && lblFleetClassInvalid.Text == "" && lblFleetYearInvalid.Text == "")
            {
                return true;
            }

            return false;
        }

        
        public bool ValidInputsCustomer()
        {
            if (lblCustomerTitleInvalid.Text == "" && lblCustomerFirstnameInvalid.Text == "" && lblCustomerLastnameInvalid.Text == ""
                && lblCustomerGenderInvalid.Text == "" && lblCustomerDOBInvalid.Text == "")
            {
                return true;
            }

            return false;
        }

        
        private void btnFleetSubmit_Click(object sender, EventArgs e)
        {
            Vehicle vehicle;
            int year;
            int numSeats;
            int dailyRate;
            bool GPS;
            bool sunroof;

            Vehicle.AutoClass vehicleClass;
            Vehicle.TypeOfFuel fuelType;
            Vehicle.TypeOfTransmission transmissionType;

            btnFleetRemove.Enabled = true;
            btnFleetModify.Enabled = true;
            btnFleetAdd.Enabled = true;

            if (!currentFleet.PresentInFleet(txtFleetRego.Text) && gbFleetAddVehicle.Text == "Add Vehicle" && ValidInputsFleet())
            {
                if(cmbFleetClass.SelectedValue.ToString() == "Economy" && cmbFleetTransmission.SelectedValue.ToString() == "Manual")
                {
                    DialogResult drConstraintViolated = MessageBox.Show(String.Format("Economy class vehicles can not have manual transmission"),
                    "Input constraint violated", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    string vehicleRego = txtFleetRego.Text;
                    string vehicleMake = txtFleetMake.Text;
                    string vehicleModel = txtFleetModel.Text;
                    Enum.TryParse(cmbFleetClass.SelectedValue.ToString(), out vehicleClass);
                    int.TryParse(txtFleetYear.Text, out year);
                    int.TryParse(nudFleetSeats.Text, out numSeats);
                    Enum.TryParse(cmbFleetTransmission.SelectedValue.ToString(), out transmissionType);
                    Enum.TryParse(cmbFleetFuel.SelectedValue.ToString(), out fuelType);

                    if (cbFleetGPS.Checked == true)
                    {
                        GPS = true;
                    }
                    else
                    {
                        GPS = false;
                    }

                    if (cbFleetSunroof.Checked == true)
                    {
                        sunroof = true;
                    }
                    else
                    {
                        sunroof = false;
                    }

                    int.TryParse(nudFleetDailyRate.Text, out dailyRate);
                    string colour = txtFleetColour.Text;

                    gbFleetAddVehicle.Visible = false;
                    vehicle = new Vehicle(vehicleRego, vehicleMake, vehicleModel, year, vehicleClass, numSeats, transmissionType, fuelType, GPS, sunroof, colour, dailyRate);
                    currentFleet.AddVehicle(vehicle);
                    currentFleet.SaveToFile();

                    ClearFleetGroupBoXAttributes();
                }
            }
            else if(currentFleet.PresentInFleet(txtFleetRego.Text) && gbFleetAddVehicle.Text == "Add Vehicle" && ValidInputsFleet())
            {
                DialogResult drRegoIsNotUnqiue = MessageBox.Show(String.Format("Vehicle Rego is already in the Fleet"),
                "Incorrect vehicle input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int rowIndex = dgvAuto.CurrentCell.RowIndex;
                currentFleet.GetFleet()[rowIndex].Make = txtFleetMake.Text;
                currentFleet.GetFleet()[rowIndex].Model = txtFleetModel.Text;
                currentFleet.GetFleet()[rowIndex].Year = int.Parse(txtFleetYear.Text);
                currentFleet.GetFleet()[rowIndex].Vehicle_Class = (Vehicle.AutoClass)cmbFleetClass.SelectedIndex;
                currentFleet.GetFleet()[rowIndex].NumSeats = int.Parse(nudFleetSeats.Value.ToString());
                currentFleet.GetFleet()[rowIndex].Fuel_Type = (Vehicle.TypeOfFuel)cmbFleetFuel.SelectedIndex;
                currentFleet.GetFleet()[rowIndex].Transmission_Type = (Vehicle.TypeOfTransmission)cmbFleetTransmission.SelectedIndex;
                currentFleet.GetFleet()[rowIndex].GPS = cbFleetGPS.Checked;
                currentFleet.GetFleet()[rowIndex].SunRoof = cbFleetSunroof.Checked;
                currentFleet.GetFleet()[rowIndex].DailyRate = double.Parse(nudFleetDailyRate.Value.ToString());
                currentFleet.GetFleet()[rowIndex].Colour = txtFleetColour.Text;
                currentFleet.SaveToFile();
                gbFleetAddVehicle.Visible = false;
                btnFleetAdd.Enabled = true;

                ClearFleetGroupBoXAttributes();                
            }

            PopulateDataGridViewFleet();
        }

        private void btnCustomerSubmit_Click(object sender, EventArgs e)
        {
            Customer customer;        
            Customer.Gender customerGender;

            btnCustomerSubmit.Enabled = true;
            btnCustomerRemove.Enabled = true;
            btnCustomerModify.Enabled = true;

            if (gbCustomerAddCustomer.Text == "Add Customer" && ValidInputsCustomer())
            {
                int customerID = int.Parse(txtCustomerID.Text);
                string title = (string)(cmbCustomerTitle.SelectedItem.ToString());
                string firstName = txtCustomerFirstname.Text;
                string lastName = txtCustomerLastname.Text;
                Enum.TryParse(cmbCustomerGender.SelectedItem.ToString(), out customerGender);
                string DOB = txtCustomerDOB.Text;

                gbCustomerAddCustomer.Visible = false;
                customer = new Customer(customerID, title, firstName, lastName, customerGender, DOB);
                currentCustomer.AddCustomer(customer);
                SetupcmbSearchCusomter();
                currentCustomer.SaveToFile();

                ClearCustomerGroupBoxAttributes();
            }
            else
            {
                int rowIndex = dgvCustomer.CurrentCell.RowIndex;
                currentCustomer.GetCustomers()[rowIndex].Title = (string)(cmbCustomerTitle.SelectedItem.ToString());
                currentCustomer.GetCustomers()[rowIndex].FirstName = txtCustomerFirstname.Text;
                currentCustomer.GetCustomers()[rowIndex].LastName = txtCustomerLastname.Text;
                currentCustomer.GetCustomers()[rowIndex].Gender_ = (Customer.Gender)cmbCustomerGender.SelectedIndex;
                currentCustomer.GetCustomers()[rowIndex].DateOfBirth = txtCustomerDOB.Text;
                currentCustomer.SaveToFile();
                gbCustomerAddCustomer.Visible = false;
                btnCustomerAdd.Enabled = true;
                SetupcmbSearchCusomter();
                currentCustomer.SaveToFile();

                ClearCustomerGroupBoxAttributes();
            }

            PopulateDataGridViewCustomer();
        }

        private void txtFleetRego_TextChanged(object sender, EventArgs e)
        {
            int validInput = 0;

            if (txtFleetRego.Text.Length == 6)
            {
                for(int i = 0; i < 6; i++)
                {
                    if(char.IsDigit(txtFleetRego.Text[i]) && i < 3)
                    {
                        validInput++;
                    }
                    if(char.IsUpper(txtFleetRego.Text[i]) && i >= 3)
                    {
                        validInput++;
                    }
                }
            }

            if ((validInput == 6 && !currentFleet.PresentInFleet(txtFleetRego.Text)) || txtFleetRego.Enabled == false)
            {
                lblFleetRegoInvalid.Text = "";
            }
            else if (validInput == 6 && currentFleet.PresentInFleet(txtFleetRego.Text))
            {
                lblFleetRegoInvalid.Text = "!";
                lblFleetRegoInvalid.ForeColor = Color.Red;
            }
            else            
            {
                lblFleetRegoInvalid.Text = "!";
                lblFleetRegoInvalid.ForeColor = Color.Red;
            }
        }

        private void txtFleetMake_TextChanged(object sender, EventArgs e)
        {
            if(txtFleetMake.Text != "")
            {
                lblFleetMakeInvalid.Text = "";
            }
            else
            {
                lblFleetMakeInvalid.Text = "!";
                lblFleetMakeInvalid.ForeColor = Color.Red;
            }
        }

        private void txtFleetModel_TextChanged(object sender, EventArgs e)
        {
            if (txtFleetMake.Text != "")
            {
                lblFleetModelInvalid.Text = "";
            }
            else
            {
                lblFleetModelInvalid.Text = "!";
                lblFleetModelInvalid.ForeColor = Color.Red;
            }
        }

        private void txtFleetYear_TextChanged(object sender, EventArgs e)
        {
            int maxYearInput = 2023;
            int minYearInput = 1800;
            int lengthOfYear = 4;
            int validInput = 0;
            bool validYear;
            int year = 0;

            if(txtFleetYear.Text.Length == lengthOfYear)
            {
                validYear = int.TryParse(txtFleetYear.Text, out year);

                for (int i = 0; i < lengthOfYear; i++)
                {
                    if (char.IsDigit(txtFleetYear.Text[i]) && validYear)
                    {
                        validInput++;
                    }
                }
            }

            if(validInput == lengthOfYear && year >= minYearInput && year <= maxYearInput)
            {
                lblFleetYearInvalid.Text = "";
            }
            else
            {
                lblFleetYearInvalid.Text = "!";
                lblFleetYearInvalid.ForeColor = Color.Red;
            }
        }


        private void cmbFleetClass_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if(cmbFleetClass.SelectedValue != null)
            {
                lblFleetClassInvalid.Text = "";
            }            
        }


        private void gbFleetAddVehicle_MouseHover(object sender, EventArgs e)
        {
            if (lblFleetClassInvalid.Text == "" && lblFleetRegoInvalid.Text == "" && lblFleetYearInvalid.Text == "" &&
            lblFleetMakeInvalid.Text == "" && lblFleetModelInvalid.Text == "" )
            {
                btnFleetSubmit.Enabled = true;
            }
            else
            {
                btnFleetSubmit.Enabled = false;
            }
        }


        private void txtCustomerDOB_TextChanged(object sender, EventArgs e)
        {
            DateTime DOB;
            if (DateTime.TryParse(txtCustomerDOB.Text, out DOB))
            {
                lblCustomerDOBInvalid.Text = "";
            }
            else
            {
                lblCustomerDOBInvalid.Text = "!";
                lblCustomerDOBInvalid.ForeColor = Color.Red;
            }
        }


        private void txtCustomerFirstname_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerFirstname.Text != "")
            {
                lblCustomerFirstnameInvalid.Text = "";
            }
            else
            {
                lblCustomerFirstnameInvalid.Text = "!";
                lblCustomerFirstnameInvalid.ForeColor = Color.Red;
            }
        }


        private void txtCustomerLastname_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomerLastname.Text != "")
            {
                lblCustomerLastnameInvalid.Text = "";
            }
            else
            {
                lblCustomerLastnameInvalid.Text = "!";
                lblCustomerLastnameInvalid.ForeColor = Color.Red;
            }
        }


        private void gbCustomerAddCustomer_MouseHover(object sender, EventArgs e)
        {
            if (lblCustomerTitleInvalid.Text == "" && lblCustomerFirstnameInvalid.Text == "" && lblCustomerLastnameInvalid.Text == "" &&
            lblCustomerGenderInvalid.Text == "" && lblCustomerDOBInvalid.Text == "")
            {
                btnCustomerSubmit.Enabled = true;
            }
            else
            {
                btnCustomerSubmit.Enabled = false;
            }
        }


        private void cmbCustomerTitle_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbCustomerTitle.SelectedItem != null)
            {
                lblCustomerTitleInvalid.Text = "";
            }
        }


        private void cmbCustomerGender_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbCustomerGender.SelectedItem != null)
            {
                lblCustomerGenderInvalid.Text = "";
            }
        }


        private void btnSearchSearch_Click(object sender, EventArgs e)
        {
            currentSearch = new Search();
            string search = cmbSearchEnterQueryHere.Text.ToLower();
            List<string> searchList = currentSearch.getSearchAttributes(search);
            List<string> operatorList = currentSearch.getOperatorList(search);
            List<List<Vehicle>> paramSearch = currentSearch.ParamSearch(searchList,
                (int)nudSearchDailyCostRangeLower.Value, (int)nudSearchDailyCostRangeUpper.Value);
            List<Vehicle> final_result = currentSearch.SearchResult(paramSearch, operatorList);
            dgvSearch.Rows.Clear();
            dgvSearch.Visible = true;
            gbSearchCreateRental.Enabled = true;

            foreach (Vehicle x in final_result)
            {
                dgvSearch.Rows.Add(x.VehicleRego, x.Model, x.Make, x.Year, x.Vehicle_Class, x.NumSeats, x.Transmission_Type, x.Fuel_Type, x.GPS, x.SunRoof, x.DailyRate, x.Colour);
            }

            if (final_result.Count == 0)
            {
                lblSearchNoMatchingVehicleFound.Visible = true;
                dgvSearch.Visible = false;
            }
        }

        private void cmbSearchEnterQueryHere_TextChanged(object sender, EventArgs e)
        {
            btnSearchSearch.Enabled = true;
        }

        private void btnSearchShowAll_Click(object sender, EventArgs e)
        {
            currentSearch = new Search();
            dgvSearch.Rows.Clear();
            List<Vehicle> result = currentFleet.GetFleet();
            result = result.Where(x => ((int)nudSearchDailyCostRangeLower.Value <= x.DailyRate) && (x.DailyRate <= (int)nudSearchDailyCostRangeUpper.Value)).ToList();
            foreach (Vehicle x in result)
            {
                dgvSearch.Rows.Add(x.VehicleRego, x.Model, x.Make, x.Year, x.Vehicle_Class, x.NumSeats, x.Transmission_Type, x.Fuel_Type, x.GPS, x.SunRoof, x.DailyRate, x.Colour);
            }
            dgvSearch.Visible = true;
            gbSearchCreateRental.Enabled = true;
        }

        private void cmbSearchEnterQueryHere_Click(object sender, EventArgs e)
        {
            cmbSearchEnterQueryHere.Text = "";
        }

        private void nudSearchRentalDuration_ValueChanged(object sender, EventArgs e)
        {
            double totalCost = (double)dgvSearch.SelectedRows[0].Cells[10].Value * (int)nudSearchRentalDuration.Value;
            lblSearchTotalCost.Text = string.Format("Total Cost: ${0}", totalCost);
            if (dgvSearch.SelectedRows.Count > 0 && cmbSearchCustomer.SelectedItem != null && (int)nudSearchRentalDuration.Value > 0) 
            {
                btnSearchRent.Enabled = true;
            }
        }

        private void btnSearchRent_Click(object sender, EventArgs e)
        {
            int customerID;
            var comboboxMessage = (string)cmbSearchCustomer.SelectedItem;
            customerID = int.Parse(comboboxMessage.Split(' ')[0]);
            string rego = (string)dgvSearch.SelectedRows[0].Cells[0].Value;
            double totalCost = (double)dgvSearch.SelectedRows[0].Cells[10].Value * (int)nudSearchRentalDuration.Value;

            DialogResult dialogResult = MessageBox.Show(string.Format("Do you want to rent vehicle {0} to  customer {1} for a total cost of ${2}?",
                                        rego, comboboxMessage, totalCost), "Rental Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                bool success = currentFleet.RentCar(rego, customerID);
                if (success)
                {
                    DialogResult dialogResult2 = MessageBox.Show("Successful");
                    PopulateDataGridViewReport();
                }
                else
                {
                    DialogResult dialogResult2 = MessageBox.Show("Unsuccessful");
                }
            }
        }

        private void dgvReport_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowsCount = dgvReport.SelectedRows.Count;

            if (rowsCount == 1)
            {
                btnReportReturnVehicle.Enabled = true;
            }
        }

        private void btnReportReturnVehicle_Click(object sender, EventArgs e)
        {
            string rego = (string)dgvReport.SelectedRows[0].Cells[0].Value;
            DialogResult dialogResult = MessageBox.Show(string.Format("Do you want return rego number {0}", rego), "Return Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                int outcome = currentFleet.ReturnCar(rego);
                if (outcome == -1)
                {
                    DialogResult dialogResult2 = MessageBox.Show("UnSuccessful");
                }
                else
                {
                    DialogResult dialogResult2 = MessageBox.Show(string.Format("Successfully returned CustomerID: {0}'s rental car", outcome));
                }
            }
            PopulateDataGridViewReport();
        }

        private void dgvReport_SelectionChanged(object sender, EventArgs e)
        {
            int rowsCount = dgvReport.SelectedRows.Count;
            if (rowsCount != 1)
            {
                btnReportReturnVehicle.Enabled = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult drSaveAllChangedData = MessageBox.Show("Would you like to save changes any changes you've made?", "Exit", MessageBoxButtons.YesNo);

            if(drSaveAllChangedData == DialogResult.Yes)
            {
                currentFleet.SaveToFile();
                currentCustomer.SaveToFile();
            }            
        }
    }
}
